# MINORITY TRANSLATE #

Minority Translate is a simple program for helping content generation on smaller sized Wikipedias (actually any sized) by giving pointers to existing articles in other language Wikipedias, so that the user can easily translate or adapt existing texts and thus increase the size and useability of their Wikipedia editions.

See the projects main homepage at http://translate.keeleleek.ee/ for more information.