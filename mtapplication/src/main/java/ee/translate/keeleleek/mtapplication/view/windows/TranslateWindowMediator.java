package ee.translate.keeleleek.mtapplication.view.windows;

import java.awt.Color;
import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.common.requests.FindReplaceRequest;
import ee.translate.keeleleek.mtapplication.common.requests.PullRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.preferences.VisualPreferences;
import ee.translate.keeleleek.mtapplication.view.application.ApplicationMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorMode;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorPosition;
import ee.translate.keeleleek.mtapplication.view.elements.EditorMediator.EditorView;
import ee.translate.keeleleek.mtapplication.view.javafx.views.AlignViewMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.views.SingleViewMediatorFX;
import ee.translate.keeleleek.mtapplication.view.javafx.views.ViewMediatorFX;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.views.ViewMediator;
import ee.translate.keeleleek.mtapplication.view.views.ViewMediator.ViewMode;
import javafx.fxml.FXML;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;


public abstract class TranslateWindowMediator extends Mediator {

	final public static String NAME = "{F0A8D0ED-D5B8-4F14-9C8B-FCEA612A1D30}";

	protected ViewMediatorFX verticalView = null;
	protected ViewMediatorFX horizontalView = null;
	protected AlignViewMediatorFX alignView = null;
	protected SingleViewMediatorFX singleView = null;
	protected ViewMediator activeView = null;
	
	protected boolean feedback = false;

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public TranslateWindowMediator() {
		super(NAME, null);
	}
	
	@Override
	public void onRegister()
	 {
		getFacade().registerMediator(createArticlesMediator());
		getFacade().registerMediator(createNotesMediator());

		showSpellCheckerBusy(false);
		showQueuerBusy(false);
		showDownloaderBusy(false);
		showPreviewerBusy(false);
		showUploaderBusy(false);
		showSessionSaveBusy(false);
		showPullingBusy(false);
		showCheckingTitleBusy(false);

		setAddonsDivider(MinorityTranslateModel.preferences().getAddonsDivider());
		setTranslateDivider(MinorityTranslateModel.preferences().getTranslateDivider());
		openView(MinorityTranslateModel.preferences().getVisual().getView());
	 }
	
	public abstract void openView(ViewMode view);
	public abstract ViewMode getView();
	
	public abstract Mediator createArticlesMediator();
	public abstract Mediator createNotesMediator();

	

	/* ******************
	 *                  *
	 *   Notification   *
	 *                  *
	 ****************** */
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.ENGINE_SELECTED_ARTICLE_CHANGED,
			Notifications.ENGINE_ARTICLE_STATUS_CHANGED,
			Notifications.ENGINE_ARTICLE_TITLE_STATUS_CHANGED,
			Notifications.ENGINE_SELECTABLE_ITEMS_CHANGED,
			Notifications.SESSION_LOADED,
			Notifications.SESSION_TAG_CHANGED,
			Notifications.SPELL_CHECKER_BUSY,
			Notifications.ENGINE_QUERER_BUSY,
			Notifications.ENGINE_DOWNLOADER_BUSY,
			Notifications.ENGINE_PREVIEWER_BUSY,
			Notifications.ENGINE_UPLOADER_BUSY,
			Notifications.SESSION_SAVING_BUSY,
			Notifications.PULLING_BUSY,
			Notifications.ENGINE_TITLE_CHECK_BUSY,
			Notifications.PROCESSING_PASTE,
			Notifications.PREFERENCES_LANGUAGES_CHANGED,
			Notifications.ARTICLE_LOADED,
			Notifications.ARTICLE_REMOVED,
			Notifications.LOGIN_STATUS_CHANGED,
			Notifications.CONNECTION_STATUS_CHANGED,
			Notifications.DOWNLOADER_STARTED,
			Notifications.DOWNLOADER_PROGRESS_CHANGED,
			Notifications.DOWNLOADER_ENDED,
			Notifications.UPLOADER_STARTED,
			Notifications.UPLOADER_PROGRESS_CHANGED,
			Notifications.UPLOADER_ENDED,
			Notifications.ENGINE_ARTICLE_TITLE_CHANGED,
			Notifications.ENGINE_ARTICLE_TEXT_CHANGED,
			Notifications.QUITTING_COMMIT,
			Notifications.FIND_FROM_ACTIVE_TAB
		};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		feedback = true;
		
		boolean busy;
		String text;
		EditorMediator tab;
		
		switch (notification.getName()) {

		case Notifications.ENGINE_SELECTED_ARTICLE_CHANGED:
			refreshNavigationControls();
			refreshUploadControls();
			refreshProcessingControls();
			refreshEditors();
			break;

		case Notifications.ENGINE_ARTICLE_STATUS_CHANGED:
		case Notifications.ENGINE_ARTICLE_TITLE_STATUS_CHANGED:
			refreshUploadControls();
			refreshProcessingControls();
			refreshCounts();
			break;

		case Notifications.ENGINE_SELECTABLE_ITEMS_CHANGED:
			refreshNavigationControls();
			break;
			
		case Notifications.SESSION_LOADED:
			refreshNavigationControls();
			refreshUploadControls();
			refreshProcessingControls();
			refreshEditors();
			refreshLogin();
			refreshWikis();
			refreshTag();
			refreshCounts();

			showSpellCheckerBusy(false);
			showQueuerBusy(false);
			showDownloaderBusy(false);
			showPreviewerBusy(false);
			showUploaderBusy(false);
			showSessionSaveBusy(false);
			showPullingBusy(false);
			showCheckingTitleBusy(false);
			
			break;

		case Notifications.SESSION_TAG_CHANGED:
			refreshTag();
			break;
			
		case Notifications.SPELL_CHECKER_BUSY:
			busy = (boolean) notification.getBody();
			showSpellCheckerBusy(busy);
			break;
			
		case Notifications.ENGINE_QUERER_BUSY:
			busy = (boolean) notification.getBody();
			showQueuerBusy(busy);
			break;

		case Notifications.ENGINE_DOWNLOADER_BUSY:
			busy = (boolean) notification.getBody();
			showDownloaderBusy(busy);
			break;

		case Notifications.ENGINE_PREVIEWER_BUSY:
			busy = (boolean) notification.getBody();
			showPreviewerBusy(busy);
			break;

		case Notifications.ENGINE_UPLOADER_BUSY:
			busy = (boolean) notification.getBody();
			showUploaderBusy(busy);
			break;

		case Notifications.SESSION_SAVING_BUSY:
			busy = (boolean) notification.getBody();
			showSessionSaveBusy(busy);
			break;

		case Notifications.PULLING_BUSY:
			busy = (boolean) notification.getBody();
			showPullingBusy(busy);
			break;

		case Notifications.ENGINE_TITLE_CHECK_BUSY:
			busy = (boolean) notification.getBody();
			showCheckingTitleBusy(busy);
			break;

		case Notifications.PROCESSING_PASTE:
			text = (String) notification.getBody();
			tab = activeView.findSelectedEditor(EditorPosition.DESTINATION);
			if (tab != null) tab.pasteText(text);
			break;

		case Notifications.PREFERENCES_LANGUAGES_CHANGED:
			refreshLogin();
			refreshWikis();
			refreshEditors();
			refreshCounts();
			break;
			
		case Notifications.ARTICLE_LOADED:
		case Notifications.ARTICLE_REMOVED:
			break;

		case Notifications.LOGIN_STATUS_CHANGED:
		case Notifications.CONNECTION_STATUS_CHANGED:
			refreshLogin();
			break;
		
		case Notifications.DOWNLOADER_STARTED:
			setProgress(0.0);
			setProgressVisible(true);
			break;
				
		case Notifications.DOWNLOADER_PROGRESS_CHANGED:
			Double progress = (Double) notification.getBody();
			setProgress(progress);
			break;
				
		case Notifications.DOWNLOADER_ENDED:
			setProgressVisible(false);
			break;

		case Notifications.UPLOADER_STARTED:
			setProgress(0.0);
			setProgressVisible(true);
			break;
				
		case Notifications.UPLOADER_PROGRESS_CHANGED:
			progress = (Double) notification.getBody();
			setProgress(progress);
			break;
				
		case Notifications.UPLOADER_ENDED:
			setProgressVisible(false);
			break;
			
		case Notifications.ENGINE_ARTICLE_TITLE_CHANGED:
		case Notifications.ENGINE_ARTICLE_TEXT_CHANGED:
			refreshCounts();
			break;

		case Notifications.QUITTING_COMMIT:
			VisualPreferences session = MinorityTranslateModel.preferences().getVisual();
			session.setAddonsDivider(getAddonsDivider());
			session.setTranslateDivider(getTranslateDivider());
			session.setView(getView());
			sendNotification(Notifications.PREFERENCES_CHANGE_VISUAL, session);
			break;

		case Notifications.FIND_FROM_ACTIVE_TAB:
			FindReplaceRequest request = (FindReplaceRequest) notification.getBody();
			tab = activeView.findSelectedEditor(EditorPosition.DESTINATION);
			if (tab != null) tab.findReplace(request);
			break;

		default:
			break;
		}
		
		feedback = false;
	 }
	
	
	/* ******************
	 *                  *
	 *     Refresh      *
	 *                  *
	 ****************** */
	protected void refreshEditors(EditorPosition pos)
	 {
		List<String> langCodes = activeView.langCodesFor(pos);
		
		// adjust tab counts
		while (activeView.getEditorCount(pos) < langCodes.size()) {
			EditorMediator editorMediator = activeView.createEditor(pos == EditorPosition.DESTINATION ? EditorMode.NORMAL : EditorMode.FILTERED, pos);
			getFacade().registerMediator(editorMediator);
		}
		while (activeView.getEditorCount(pos) > langCodes.size()){
			EditorMediator editorMediator = activeView.destroyEditor(pos);
			getFacade().removeMediator(editorMediator.getMediatorName());
		}
		
		// change tabs
		String qid = MinorityTranslateModel.content().getSelectedQid();
		
		
		for (int i = 0; i < langCodes.size(); i++) {
			
			String langCode = langCodes.get(i);
			EditorMediator editor = activeView.getEditor(pos, i);
			
			String editorName = langCode;
			String langName = MinorityTranslateModel.wikis().getLangName(langCode);
			if (langName != null) editorName = langName + " (" + editorName + ")";
			editor.setName(editorName);
			
			editor.changeReference(qid, langCode);
			
			activeView.setEditorName(pos, i, editorName);
			activeView.markEditorNew(pos, i, editor.isArticleNew());
			
		}
	 }
	
	private void refreshEditors()
	 {
		refreshEditors(EditorPosition.DESTINATION);
		refreshEditors(EditorPosition.SOURCE);
	 }
	
	private void refreshNavigationControls()
	 {
		boolean hasArticles = MinorityTranslateModel.content().getSelectableCount() > 0;
		setNavigationDisable(!hasArticles);
		
		disableMenuExactToggle(!hasArticles);
	 }
	
	private void refreshUploadControls()
	 {
		// upload button
		boolean isSelected = MinorityTranslateModel.content().getSelectedQid() != null;
		setUploadDisable(!isSelected);
		
		String qid = MinorityTranslateModel.content().getSelectedQid();
		boolean selected = false;
		if (qid != null) selected = MinorityTranslateModel.content().hasStatus(qid, Status.UPLOAD_REQUESTED);
		setUpload(selected);
		
		// menu
		boolean uploadEnabled = MinorityTranslateModel.session().isUploadEnabled();
		setMenuUploadEnabled(uploadEnabled);
	 }
	
	private void refreshProcessingControls()
	 {
		// upload button
		boolean isSelected = MinorityTranslateModel.content().getSelectedQid() != null;
		setPullDisable(!isSelected);
	 }

	private void refreshLogin()
	 {
		// Connection:
		if (!MinorityTranslateModel.connection().hasConnection()) {
			setLoginIndicator(Color.RED, Messages.getString("statusbar.no.connection"));
			return;
		}
		
		// Login:
		MediaWikiBot[] bots = MinorityTranslateModel.bots().fetchToBots();
		for (int i = 0; i < bots.length; i++) {
			
			if (!bots[i].isLoggedIn()) {
				setLoginIndicator(Color.YELLOW, Messages.getString("statusbar.not.logged.in"));
				return;
			}
			
		}
		
		// No problems:
		setLoginIndicator(Color.GREEN, Messages.getString("statusbar.logged.in"));
	 }
	
	private void refreshWikis()
	 {
		showWikis(MinorityTranslateModel.preferences().languages().getAllLangCodes());
	 }

	private void refreshTag()
	 {
		String tag = MinorityTranslateModel.session().getTag();
		if (!tag.isEmpty()) tag = "[" + tag + "]";
		showTag(tag);
	 }

	private void refreshCounts()
	 {
		int doneCount = MinorityTranslateModel.content().countDoneDstArticles();
		int totCount = MinorityTranslateModel.content().countDstArticles();
		showCount(doneCount + "/" + totCount);
	 }
	
	
	/* ******************
	 *                  *
	 *    Languages     *
	 *                  *
	 ****************** */
	public String findSelectedSrcLangCode() {
		return activeView.findSelectedSrcLangCode();
	}
	
	
	/* ******************
	 *                  *
	 *       Tabs       *
	 *                  *
	 ****************** */
	
	
	// current tab
	private EditorMediator getFocusedTab()
	 {
		List<EditorMediator> editors;

		editors = activeView.getEditors(EditorPosition.DESTINATION);
		for (EditorMediator tab : editors) {
			if (tab.isEditFocused()) return tab;
		}

		editors = activeView.getEditors(EditorPosition.SOURCE);
		for (EditorMediator tab : editors) {
			if (tab.isEditFocused()) return tab;
		}
		
		return null;
	 }
	
	
	public Reference getCurrentReference()
	 {
		EditorMediator tab = activeView.findSelectedEditor(EditorPosition.DESTINATION);
		if (tab == null) return null;
		return tab.getReference();
	 }
	
	private void togglePreview()
	 {
		EditorMediator tab = getFocusedTab();
		if (tab == null) return;
		
		switch (tab.getTabView()) {
		case EDIT:
			tab.onChangeView(EditorView.PREVIEW);
			break;
			
		case PREVIEW:
			tab.onChangeView(EditorView.EDIT);
			break;

		default:
			break;
		}
	 }
	
	
	
	/* ******************
	 *                  *
	 *  User interface  *
	 *                  *
	 ****************** */
	// menu
	protected abstract void setMenuUploadEnabled(boolean enabled);
	protected abstract void disableMenuExactToggle(boolean disable);
	
	protected abstract void setLoginIndicator(Color colour, String tooltip);

	
	// toolbar
	protected abstract void setNavigationDisable(boolean disabled);
	
	protected abstract boolean isUpload();
	protected abstract void setUploadDisable(boolean disabled);
	protected abstract void setUpload(boolean selected);
	
	protected abstract String getPullPluginName();
	protected abstract void setPullDisable(boolean disabled);

	// dividers
	public abstract int getAddonsDivider();
	public abstract void setAddonsDivider(int position);
	public abstract double getTranslateDivider();
	public abstract void setTranslateDivider(double position);
	
	// statusbar
	protected abstract void showWikis(List<String> langCodes);
	protected abstract void showTag(String tag);
	protected abstract void showCount(String count);
	
	protected abstract void setProgressVisible(boolean visible);
	protected abstract void setProgress(double value);

	protected abstract void showSpellCheckerBusy(boolean busy);
	protected abstract void showQueuerBusy(boolean busy);
	protected abstract void showDownloaderBusy(boolean busy);
	protected abstract void showPreviewerBusy(boolean busy);
	protected abstract void showUploaderBusy(boolean busy);
	protected abstract void showSessionSaveBusy(boolean busy);
	protected abstract void showPullingBusy(boolean busy);
	protected abstract void showCheckingTitleBusy(boolean busy);
	
	
	
	/* ******************
	 *                  *
	 *      Events      *
	 *                  *
	 ****************** */
	// session
	public void onSessionNew() {
		sendNotification(Notifications.SESSION_NEW_REQUEST);
	}

	public void onSessionOpen() {
		sendNotification(Notifications.MENU_SESSION_OPEN);
	}

	public void onSessionSave() {
		sendNotification(Notifications.MENU_SESSION_SAVE);
	}

	public void onSessionSaveAs() {
		sendNotification(Notifications.MENU_SESSION_SAVE_AS);
	}

	// preferences
	public void onPreferences() {
		sendNotification(Notifications.MENU_PREFERENCES_OPEN);
	}

	// log in
	public void onLogin() {
		sendNotification(Notifications.MENU_LOGIN_OPEN);
	}

	public void onLogout() {
		sendNotification(Notifications.LOGOUT);
	}

	// quit
	public void onQuit() {
		sendNotification(Notifications.REQUEST_QUIT);
	}


	public void onUploadEnable(boolean enabled) {
		sendNotification(Notifications.CHANGE_UPLOAD_ENABLED, enabled);
	}
	
	public void onUpload() {
		onUpload(true);
	}
	
	public void onAddCategory()
	 {
		if (MinorityTranslateModel.preferences().languages().getAllLangCodes().size() == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_ADD_CATEGORY_OPEN);
	 }

	public void onAddLinks()
	 {
		if (MinorityTranslateModel.preferences().languages().getLangCodeCount() == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_ADD_LINKS_OPEN);
	 }

	public void onAddArticle()
	 {
		if (MinorityTranslateModel.preferences().languages().getLangCodeCount() == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_ADD_ARTICLE_OPEN);
	 }

	public void onAddContent()
	 {
		List<ContentRequest> requests = DialogFactory.dialogs().askAddContent();
		for (ContentRequest request : requests) {
			
			sendNotification(Notifications.ADD_CONTENT, request);
			
		}
	 }
	
	public void onStopDownloading() {
		sendNotification(Notifications.DOWNLOADER_STOP);
	}
	
	public void onLists()
	 {
		if (MinorityTranslateModel.preferences().languages().getLangCodeCount() == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_LISTS_OPEN);
	 }

	public void onRemoveArticle()
	 {
		if (MinorityTranslateModel.preferences().languages().getLangCodeCount() == 0) {
			DialogFactory.dialogs().showError(Messages.getString("messages.adding.header.no.languages"), Messages.getString("messages.adding.content.add.one.language"));
			return;
		}
		sendNotification(Notifications.MENU_REMOVE_ARTICLE_OPEN);
	 }

	public void onRemoveSelectedArticle() {
		sendNotification(Notifications.MENU_REMOVE_SELECTED_ARTICLE);
	}
	
	public void onAbout() {
		sendNotification(Notifications.MENU_ABOUT_OPEN);
	}

	public void onManual() {
		sendNotification(Notifications.MENU_MANUAL_OPEN);
	}

	public void onResetQuickStart()
	 {
		sendNotification(Notifications.RESET_QUICK_START);
		sendNotification(Notifications.SHOW_QUICK_START);
	 }
	
	
	// toolbar
	public void onNext() {
		sendNotification(Notifications.MENU_SELECT_NEXT_ARTICLE);
	}

	public void onPrevious() {
		sendNotification(Notifications.MENU_SELECT_PREVIOUS_ARTICLE);
	}

	
	public void onNextArticle() {
		sendNotification(Notifications.MENU_SELECT_NEXT_ARTICLE);
	}

	public void onPreviousArticle() {
		sendNotification(Notifications.MENU_SELECT_PREVIOUS_ARTICLE);
	}

	public void onTogglePreview() {
		togglePreview();
	}

	public void onToggleExact()
	 {
		EditorMediator tab = activeView.findSelectedEditor(EditorPosition.DESTINATION);
		if (tab == null) return;
		
		Reference ref = tab.getReference();
		boolean exact = MinorityTranslateModel.content().isExact(ref);
		
		sendNotification(Notifications.ENGINE_CHANGE_ARTICLE_EXACT, ref, (!exact) + "");
	 }
	
	
	public void onUpload(boolean upload)
	 {
		String qid = MinorityTranslateModel.content().getSelectedQid();
		if (qid == null) return;
		
		activeView.commitEditors();
		
		if (upload) sendNotification(Notifications.ENGINE_REQUEST_UPLOAD_QID, qid);
		else  sendNotification(Notifications.ENGINE_CANCEL_REQUEST_UPLOAD_QID, qid);
		
		refreshUploadControls();
	 }

	
	public void onPull()
	 {
		EditorMediator srcTab = activeView.findSelectedEditor(EditorPosition.SOURCE);
		EditorMediator dstTab = activeView.findSelectedEditor(EditorPosition.DESTINATION);

		Reference srcRef = null;
		if (srcTab != null) srcRef = srcTab.getReference();
		if (srcRef == null) return;
		
		Reference dstRef = null;
		if (dstTab != null) dstRef = dstTab.getReference();
		if (dstRef == null) return;
		
		String pluginName = getPullPluginName();
		if (pluginName == null) return;
		
		activeView.commitEditors();
		
		sendNotification(Notifications.REQUEST_PULL, new PullRequest(pluginName, srcRef, dstRef));
	 }
	
	public void onTag()
	 {
		String tag = DialogFactory.dialogs().askTag();
		if (tag != null) sendNotification(Notifications.SESSION_CHANGE_TAG, tag);
	 }
	
	
	// EVENTS (STATUSBAR)
	public void onOpenWiki(String langCode)
	 {
		String url;
		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(langCode);
		switch (wikiType) {
		case REGULAR:
			url = "https://" + langCode + ".wikipedia.org";
			break;
		
		case INCUBATOR:
			url = "https://incubator.wikimedia.org";
			break;
			
		default:
			return;
		}
		
		String username = MinorityTranslateModel.preferences().credentials().getUsername();
		if (username != null) url+= "/wiki/Special:Contributions/" + username;

		ApplicationMediator mediator = (ApplicationMediator) getFacade().retrieveMediator(ApplicationMediator.NAME);
		mediator.showDocument(url);
	 }

	
	// EVENTS (VIEWS)
	@FXML
	public void onHorizontalView()
	 {
		openView(ViewMode.SPLIT_HORIZONTAL);
		refreshEditors();
	 }
	
	@FXML
	public void onVerticalView()
	 {
		openView(ViewMode.SPLIT_VERTICAL);
		refreshEditors();
	 }

	public void onAlignView()
	 {
		openView(ViewMode.ALIGN);
		refreshEditors();
	 }

	public void onSingleView()
	 {
		openView(ViewMode.SINGLE);
		refreshEditors();
	 }
	
	
}
