package ee.translate.keeleleek.mtapplication.controller.populating;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class RemoveSelectedArticleCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		String qid = MinorityTranslateModel.content().getSelectedQid();
		
		if (qid != null) MinorityTranslateModel.content().removeQid(qid);
	 }
	
}
