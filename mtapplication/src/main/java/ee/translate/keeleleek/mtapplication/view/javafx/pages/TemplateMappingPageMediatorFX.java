package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import java.util.ArrayList;

import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMapping;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMappingPreferences;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.TemplateMappingFX;
import ee.translate.keeleleek.mtapplication.view.pages.TemplateMappingPageMediator;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.util.Callback;


public class TemplateMappingPageMediatorFX extends TemplateMappingPageMediator {

	@FXML
	private TableView<TemplateMappingFX> mappingsTable;
	@FXML
	private TableColumn<TemplateMappingFX, String> srcColumn;
	@FXML
	private TableColumn<TemplateMappingFX, String> dstColumn;
	@FXML
	private TableColumn<TemplateMappingFX, String> filterColumn;

	@FXML
	private Button insertButton;
	@FXML
	private Button appendButton;
	@FXML
	private Button moveUpButton;
	@FXML
	private Button moveDownButton;
	@FXML
	private Button removeButton;

	@FXML
	private TextField srcLangEdit;
	@FXML
	private TextField dstLangEdit;
	@FXML
	private TextField srcTemplateEdit;
	@FXML
	private TextField srcParameterEdit;
	@FXML
	private TextField dstParameterEdit;
	
	
	// IMPLEMENT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		srcColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TemplateMappingFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<TemplateMappingFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(Bindings.concat(mapping.getValue().srcTemplate, " : ", mapping.getValue().srcParameter));
				return prop;
			}
		});

		dstColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TemplateMappingFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<TemplateMappingFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(Bindings.concat(mapping.getValue().dstParameter));
				return prop;
			}
		});

		filterColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TemplateMappingFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<TemplateMappingFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(Bindings.concat(mapping.getValue().srcLang, " \u2192 ", mapping.getValue().dstLang));
				return prop;
			}
		});
		
		mappingsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TemplateMappingFX>() {
			@Override
			public void changed(ObservableValue<? extends TemplateMappingFX> obs, TemplateMappingFX oldVal, TemplateMappingFX newVal)
			 {
				if (oldVal != null) {
					oldVal.srcLang.unbind();
					oldVal.dstLang.unbind();
					oldVal.srcTemplate.unbind();
					oldVal.srcParameter.unbind();
					oldVal.dstParameter.unbind();
				}
				if (newVal != null) {

					srcLangEdit.setText(newVal.srcLang.getValue());
					newVal.srcLang.bind(srcLangEdit.textProperty());

					dstLangEdit.setText(newVal.dstLang.getValue());
					newVal.dstLang.bind(dstLangEdit.textProperty());

					srcTemplateEdit.setText(newVal.srcTemplate.getValue());
					newVal.srcTemplate.bind(srcTemplateEdit.textProperty());

					srcParameterEdit.setText(newVal.srcParameter.getValue());
					newVal.srcParameter.bind(srcParameterEdit.textProperty());
					
					dstParameterEdit.setText(newVal.dstParameter.getValue());
					newVal.dstParameter.bind(dstParameterEdit.textProperty());
					
				}
			 }
		});
	
		mappingsTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TemplateMappingFX>() {
			@Override
			public void changed(ObservableValue<? extends TemplateMappingFX> obs, TemplateMappingFX oldVal, TemplateMappingFX newVal)
			 {
				appendButton.setDisable(false);
				insertButton.setDisable(false);
				moveUpButton.setDisable(newVal == null);
				moveUpButton.setDisable(newVal == null);
				moveDownButton.setDisable(newVal == null);
				removeButton.setDisable(newVal == null);
				srcLangEdit.setDisable(newVal == null);
				dstLangEdit.setDisable(newVal == null);
				srcTemplateEdit.setDisable(newVal == null);
				srcParameterEdit.setDisable(newVal == null);
				dstParameterEdit.setDisable(newVal == null);
				if (newVal == null) {
					srcLangEdit.setText("");
					dstLangEdit.setText("");
					srcTemplateEdit.setText("");
					srcParameterEdit.setText("");
					dstParameterEdit.setText("");
				}
			 }
		});
		
	 }

	
	// PAGE
	@Override
	public void open(TemplateMappingPreferences mappings)
	 {
		ObservableList<TemplateMappingFX> mappingListFX = FXCollections.observableArrayList();
		for (int i = 0; i < mappings.getMappingCount(); i++) {
			mappingListFX.add(new TemplateMappingFX(mappings.getMapping(i)));
		}
		
		mappingsTable.setItems(mappingListFX);

		srcLangEdit.setText("");
		dstLangEdit.setText("");
		srcTemplateEdit.setText("");
		srcParameterEdit.setText("");
		dstParameterEdit.setText("");
		
		mappingsTable.getSelectionModel().selectFirst();
	 }
	
	@Override
	public TemplateMappingPreferences close()
	 {
		ObservableList<TemplateMappingFX> mappingListFX = mappingsTable.getItems();
		ArrayList<TemplateMapping> mappingList = new ArrayList<>(mappingListFX.size());
		for (TemplateMappingFX mappingFX : mappingListFX) {
			mappingList.add(mappingFX.toMapping());
		}
		
		return new TemplateMappingPreferences(mappingList);
	 }
	
	
	// TABLE
	private void addRow(int index)
	 {
		ObservableList<TemplateMappingFX> items = mappingsTable.getItems();
		if (index > items.size()) index = items.size();
		if (index < 0) index = 0;
		
		items.add(index, new TemplateMappingFX(new TemplateMapping()));
		mappingsTable.getSelectionModel().select(items.get(index));
	 }
	
	private void removeRow(int index)
	 {
		ObservableList<TemplateMappingFX> items = mappingsTable.getItems();
		if (index >= items.size()) return;
		if (index < 0) return;

		boolean select = mappingsTable.getSelectionModel().getSelectedIndex() != -1;
		
		items.remove(index);
		
		if (select) {
			if (index == items.size()) index--;
			mappingsTable.getSelectionModel().select(index);
		}
	 }

	private void moveRowDown(int index)
	 {
		ObservableList<TemplateMappingFX> items = mappingsTable.getItems();
		if (index >= items.size() - 1) return;
		if (index < 0) return;

		boolean select = mappingsTable.getSelectionModel().getSelectedIndex() != -1;
		
		TemplateMappingFX item = items.remove(index);
		items.add(index + 1, item);

		if (select) {
			mappingsTable.getSelectionModel().select(item);
		}
	 }

	private void moveRowUp(int index)
	 {
		ObservableList<TemplateMappingFX> items = mappingsTable.getItems();
		if (index >= items.size()) return;
		if (index < 1) return;

		boolean select = mappingsTable.getSelectionModel().getSelectedIndex() != -1;
		
		TemplateMappingFX item = items.remove(index);
		items.add(index - 1, item);

		if (select) {
			mappingsTable.getSelectionModel().select(item);
		}
	 }
	
	
	// ACTIONS
	@FXML
	private void onAppendClick() {
		addRow(mappingsTable.getSelectionModel().getSelectedIndex() + 1);
	}
	
	@FXML
	private void onInsertClick() {
		addRow(mappingsTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveUpClick() {
		moveRowUp(mappingsTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveDownClick() {
		moveRowDown(mappingsTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onRemoveClick() {
		removeRow(mappingsTable.getSelectionModel().getSelectedIndex());
	}

	
}
