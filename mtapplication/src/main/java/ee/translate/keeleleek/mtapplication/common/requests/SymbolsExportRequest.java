package ee.translate.keeleleek.mtapplication.common.requests;

import java.nio.file.Path;
import java.util.List;

import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;

public class SymbolsExportRequest {

	private String target;
	private Path path;
	private List<Symbol> symbols;
	
	
	public SymbolsExportRequest(String target, Path path, List<Symbol> symbols) {
		super();
		this.target = target;
		this.path = path;
		this.symbols = symbols;
	}
	
	
	public SymbolsExportResponse fulfill()
	 {
		return new SymbolsExportResponse(target);
	 }
	
	
	public String getTarget() {
		return target;
	}
	
	public Path getPath() {
		return path;
	}
	
	public List<Symbol> getSymbols() {
		return symbols;
	}
	
	
	public static class SymbolsExportResponse {
		
		private String target;
		
		
		public SymbolsExportResponse(String target) {
			super();
			this.target = target;
		}
		
		
		public String getTarget() {
			return target;
		}
		
	}
	
}
