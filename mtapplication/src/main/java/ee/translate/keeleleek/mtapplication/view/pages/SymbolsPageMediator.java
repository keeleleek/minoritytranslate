package ee.translate.keeleleek.mtapplication.view.pages;

import java.nio.file.Path;
import java.util.List;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.SymbolsExportRequest;
import ee.translate.keeleleek.mtapplication.common.requests.SymbolsImportRequest;
import ee.translate.keeleleek.mtapplication.common.requests.SymbolsImportRequest.SymbolsImportResponse;
import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;
import ee.translate.keeleleek.mtapplication.model.preferences.SymbolsPreferences;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.javafx.dialogs.DialogFactoryFX;


public abstract class SymbolsPageMediator extends Mediator implements PreferencesPage<SymbolsPreferences> {

	public final static String NAME = "{E654DED1-F433-4CBF-A8D7-C16E3C41D3B5}";
	
	
	// INIT
	public SymbolsPageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	@Override
	public String[] listNotificationInterests() {
		return new String[]
		 {
			Notifications.REQUEST_FULFILLED
		 };
	}
	

	@Override
	public void handleNotification(INotification notification)
	 {
		switch (notification.getName()) {
		case Notifications.REQUEST_FULFILLED:
			
			if (notification.getBody() instanceof SymbolsImportResponse) {
				
				SymbolsImportResponse importResponse = (SymbolsImportResponse) notification.getBody();
				
				if (importResponse.getTarget().equals(NAME)) {
					List<Symbol> symbols = importResponse.getSymbols();
					for (Symbol symbol : symbols) {
						addSymbol(symbol.getSymbol());
					}
				}
				
			}
			
			break;
				
		 default:
			break;
		}
	 }
	
	
	// SYMBOLS
	public String askSymbols()
	 {
		return DialogFactoryFX.dialogs().askUnicodeCharacter();
	 }
	
	
	// INHERIT
	public abstract void addSymbol(char symbol);

	
	// EVENTS
	protected void onImportSymbolSet()
	 {
		Path path = DialogFactory.dialogs().askSymbolsOpenPath();
		if (path == null) return;
		getFacade().sendNotification(Notifications.REQUEST, new SymbolsImportRequest(NAME, path));
	 }

	protected void onExportSymbolSet(List<Symbol> symbols)
	 {
		Path path = DialogFactory.dialogs().askSymbolsSavePath();
		if (path == null) return;
		getFacade().sendNotification(Notifications.REQUEST, new SymbolsExportRequest(NAME, path, symbols));
	 }
	
	
}
