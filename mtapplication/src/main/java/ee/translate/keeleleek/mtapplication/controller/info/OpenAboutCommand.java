package ee.translate.keeleleek.mtapplication.controller.info;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;

public class OpenAboutCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		getFacade().sendNotification(Notifications.WINDOW_ABOUT_OPEN, true);
	 }
	
}
