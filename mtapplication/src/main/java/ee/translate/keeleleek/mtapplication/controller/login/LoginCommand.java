package ee.translate.keeleleek.mtapplication.controller.login;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class LoginCommand extends SimpleCommand {

	protected static Logger LOGGER = LoggerFactory.getLogger(LoginCommand.class);

	
	@Override
	public void execute(INotification notification)
	 {
		final String username;
		final String password;
		final Boolean auto;
		
		if (notification.getType() != null) {
			
			String[] split = notification.getType().split("\n");
			if (split.length != 2) {
				LOGGER.error("Incorrect split " + notification.getType() + "!");
				return;
			}
			username = split[0];
			password = split[1];
			
			auto = (Boolean)notification.getBody();
			
			MinorityTranslateModel.login().logIn(username, password, auto);
			
		} else {

			MinorityTranslateModel.login().logIn();
			
		}
	 }
	
}
