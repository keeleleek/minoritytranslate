package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.CorpusPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LanguagesPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Display;
import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Proficiency;
import ee.translate.keeleleek.mtapplication.model.preferences.TranslatePreferences;
import ee.translate.keeleleek.mtapplication.view.application.ApplicationMediator;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtapplication.view.pages.TranslatePageMediator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;


public class TranslatePageMediatorFX extends TranslatePageMediator {
	
	@FXML
	private CheckBox collectSelect;
	@FXML
	private RadioButton adaptationSelect;
	@FXML
	private RadioButton exactSelect;
	@FXML
	private GridPane languagesGrid;
	@FXML
	private TextField languageEdit;
	@FXML
	private ContextMenu languageEditMenu;

	private ArrayList<String> langCodes = new ArrayList<>();
	private HashMap<String, Label> langCache = new HashMap<>();
	private HashMap<String, ComboBox<String>> displayCache = new HashMap<>();
	private HashMap<String, ComboBox<String>> proficiencyCache = new HashMap<>();
	private HashMap<String, Button> removeButtonCache = new HashMap<>();
	

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		languageEdit.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue)
			 {
				showLanguageSuggestions(MinorityTranslateModel.wikis().findSuggestions(newValue));
			 }
		});
	 }

	
	
	/* ******************
	 *                  *
	 *       Page       *
	 *                  *
	 ****************** */
	@Override
	public void open(TranslatePreferences preferences)
	 {
		List<String> langCodes = preferences.languages().getAllLangCodes();
		
		for (String langCode : langCodes) {
			
			Display display = preferences.languages().getDisplay(langCode);
			Proficiency proficiency = preferences.corpus().getProficiency(langCode);
			
			addLanguage(langCode, display, proficiency);
			
		}
		
		collectSelect.setSelected(preferences.corpus().isCollect());
		adaptationSelect.setSelected(!preferences.corpus().isExact());
		exactSelect.setSelected(preferences.corpus().isExact());
	 }
	
	@Override
	public TranslatePreferences close()
	 {
		HashMap<String, Display> displays = new HashMap<>();
		HashMap<String, Proficiency> proficiencies = new HashMap<>();
		
		for (String langCode : langCodes) {
			
			Display display = Display.NONE;
			Proficiency proficiency = Proficiency.UNSPECIFIED;
			
			ComboBox<String> displayCombo = displayCache.get(langCode);
			if (displayCombo != null) display = Display.values()[displayCombo.getSelectionModel().getSelectedIndex()];
			
			ComboBox<String> proficiencyCombo = proficiencyCache.get(langCode);
			if (proficiencyCombo != null) proficiency = Proficiency.values()[proficiencyCombo.getSelectionModel().getSelectedIndex()];
			
			displays.put(langCode, display);
			proficiencies.put(langCode, proficiency);
			
		}
		
		return new TranslatePreferences(new LanguagesPreferences(langCodes, displays), new CorpusPreferences(proficiencies, collectSelect.isSelected(), exactSelect.isSelected()));
	 }
	
	
	
	/* ******************
	 *                  *
	 *    Languages     *
	 *                  *
	 ****************** */
	private void addLanguage(String langCode, Display display, Proficiency proficiency)
	 {
		removeLanguage(langCode);
		
		int r = findNumRows(languagesGrid);
		
		// language name
		String langName = MinorityTranslateModel.wikis().getLangName(langCode);
		if (langName == null) langName = langCode;
		Label langLabel = new Label(langName);
		
		// display
		ObservableList<String> dispList = FXCollections.observableArrayList(
				Messages.getString("preferences.program.languages.display.from"),
				Messages.getString("preferences.program.languages.display.to"),
				Messages.getString("preferences.program.languages.display.none")
		);
		final ComboBox<String> displaySelect = new ComboBox<>(dispList);
		displaySelect.setMaxWidth(Double.MAX_VALUE);
		displaySelect.getSelectionModel().select(display.ordinal());
		displaySelect.setTooltip(new Tooltip(Messages.getString("preferences.program.languages.display.tooltip")));
		
		// proficiency
		ObservableList<String> proficiencies = FXCollections.observableArrayList(
				Messages.getString("preferences.program.corpus.proficiency.unspecified").replace("#langCode", langCode),
				Messages.getString("preferences.program.corpus.proficiency.level1"     ).replace("#langCode", langCode),
				Messages.getString("preferences.program.corpus.proficiency.level2"     ).replace("#langCode", langCode),
				Messages.getString("preferences.program.corpus.proficiency.level3"     ).replace("#langCode", langCode),
				Messages.getString("preferences.program.corpus.proficiency.level4"     ).replace("#langCode", langCode),
				Messages.getString("preferences.program.corpus.proficiency.level5"     ).replace("#langCode", langCode)
		);
		final ComboBox<String> proficiencySelect = new ComboBox<>(proficiencies);
		proficiencySelect.setMaxWidth(Double.MAX_VALUE);
		proficiencySelect.getSelectionModel().select(proficiency.ordinal());
		
		// remove button
		Button removeButton = new Button("\u2A2F");
		removeButton.setMaxHeight(20);
		removeButton.setMaxWidth(20);
		removeButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				removeLanguage(langCode);
			}
		});
		
		langLabel.setMinHeight(48); // add spacing between language elements
		
		languagesGrid.addRow(r, langLabel, displaySelect, proficiencySelect, removeButton);

		langCache.put(langCode, langLabel);
		displayCache.put(langCode, displaySelect);
		proficiencyCache.put(langCode, proficiencySelect);
		removeButtonCache.put(langCode, removeButton);
		langCodes.add(langCode);
	 }
	
	private void addLanguage(String langCode)
	 {
		// recall display
		Display display = Display.NONE;
		if (displayCache.containsKey(langCode)) display = Display.values()[displayCache.get(langCode).getSelectionModel().getSelectedIndex()];

		// recall proficiency
		Proficiency proficiency = Proficiency.UNSPECIFIED;
		if (proficiencyCache.containsKey(langCode)) proficiency = Proficiency.values()[proficiencyCache.get(langCode).getSelectionModel().getSelectedIndex()];
		
		addLanguage(langCode, display, proficiency);
	 }
	
	private void removeLanguage(String langCode)
	 {
		Button removeButton = removeButtonCache.get(langCode);
		if (removeButton != null) {
			removeButtonCache.remove(removeButton);
			languagesGrid.getChildren().remove(removeButton);
		}

		ComboBox<String> proficiencySelect = proficiencyCache.get(langCode);
		if (proficiencySelect != null) {
			proficiencyCache.remove(langCode);
			languagesGrid.getChildren().remove(proficiencySelect);
		}

		ComboBox<String> displaySelect = displayCache.get(langCode);
		if (displaySelect != null) {
			displayCache.remove(langCode);
			languagesGrid.getChildren().remove(displaySelect);
		}
		
		Label langLabel = langCache.get(langCode);
		if (langLabel != null) {
			langCache.remove(langCode);
			languagesGrid.getChildren().remove(langLabel);
		}

		langCodes.remove(langCode);
	 }
	
	
	
	/* ******************
	 *                  *
	 *   Suggestions    *
	 *                  *
	 ****************** */
	private void showLanguageSuggestions(String[] suggestions)
	 {
		languageEditMenu.getItems().clear();
		
		for (int i = 0; i < suggestions.length; i++) {
			
			final String suggestion = suggestions[i];
			
			MenuItem item = new MenuItem(suggestions[i]);
			item.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					languageEdit.setText(suggestion);
					languageEdit.positionCaret(suggestion.length());
				}
			});
			
			languageEditMenu.getItems().add(item);
			
		}
		
		if (suggestions.length > 0){
			languageEditMenu.show(languageEdit, Side.BOTTOM, 0, 0);
		} else {
			languageEditMenu.hide();
		}
	 }
	
	
	
	/* ******************
	 *                  *
	 *    GUI Events    *
	 *                  *
	 ****************** */
	@FXML
	private void onAddLanguageClick()
	 {
		String langCode = MinorityTranslateModel.wikis().findLangCode(languageEdit.getText());
		if (langCode == null) return;
		addLanguage(langCode);
		languageEdit.clear();
	 }
	
	@FXML
	private void onKeeleleekClick()
	 {
		ApplicationMediator.application().showDocument(Messages.getString("preferences.program.corpus.link"));
	 }
	

	/* ******************
	 *                  *
	 *     Helpers      *
	 *                  *
	 ****************** */
	// http://stackoverflow.com/questions/20766363/get-the-number-of-rows-in-a-javafx-gridpane
	private static int findNumRows(GridPane pane)
	 {
		int numRows = pane.getRowConstraints().size();
        for (int i = 0; i < pane.getChildren().size(); i++) {
            Node child = pane.getChildren().get(i);
            if (child.isManaged()) {
                Integer rowIndex = GridPane.getRowIndex(child);
                if(rowIndex != null){
                    numRows = Math.max(numRows,rowIndex+1);
                }
            }
        }
        return numRows;
	 }
	
	
}
