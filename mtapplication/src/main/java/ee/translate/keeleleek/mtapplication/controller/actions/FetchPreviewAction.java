package ee.translate.keeleleek.mtapplication.controller.actions;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;

import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.javafx.application.ApplicationMediatorFX;
import net.sourceforge.jwbf.core.actions.Post;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.ApiRequestBuilder;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class FetchPreviewAction extends MWAction {

	private final static String PREVIEW_HTML_TEMPLATE = readTemplate();

	private HttpAction action;
	
	private String text;
	
	private String html = null;
	
	
	public FetchPreviewAction(String text)
	 {
		this.text = text;
		action = getAction();
	 }
	
	public HttpAction getAction()
	 {
		Post action = new ApiRequestBuilder()
			.action("parse")
			.postParam("text", text)
			.postParam("format", "xml")
			.postParam("disabletoc", "true")
			.postParam("disablepp", "true")
			.postParam("preview", "true")
			.postParam("contentmodel", "wikitext")
			.postParam("disableeditsection", "true")
			.buildPost();
			
		return action;
	 }
	
	@Override
	public HttpAction getNextMessage() {
		HttpAction action = this.action;
		this.action = null;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return action != null;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		Pattern pattern = Pattern.compile("<text.*?>(.*?)</text>", Pattern.DOTALL | Pattern.MULTILINE);
	    Matcher matcher = pattern.matcher(response);
	    if (!matcher.find()) return super.processReturningText(response, action);
	    
	    html = matcher.group(1);
	    html = StringEscapeUtils.unescapeHtml3(html);
	    html = html.replace("//upload.wikimedia.org", "https://upload.wikimedia.org");
	    html = PREVIEW_HTML_TEMPLATE.replace("${CONTENT}", html);
	    html = html.replace("${FONT_CSS_URL}", getClass().getResource(ApplicationMediatorFX.findFontSizeCSS(MinorityTranslateModel.preferences().getFontSize())).toExternalForm());
	    
		return super.processReturningText(response, action);
	 }
	
	/**
	 * Gets the parsed text.
	 * 
	 * @return parsed text
	 */
	public String getParsedText() {
		return html;
	}
	
	
	private static String readTemplate() {
		String template;
		try {
			template = FileUtil.readFromJar("/previewview.html");
		} catch (IOException e) {
			e.printStackTrace(); // should not happen
			template = "";
		}
		return template;
	}
	
}
