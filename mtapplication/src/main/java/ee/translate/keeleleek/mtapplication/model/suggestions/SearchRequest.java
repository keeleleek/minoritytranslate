package ee.translate.keeleleek.mtapplication.model.suggestions;

public class SearchRequest {

	public enum RequestType { ARTICLES, CATEGORIES, LOADED_ARTICLES, ARTICLE_LINKS }
	
	private String langCode;
	private RequestType type;
	private String title;
	
	
	public SearchRequest(String langCode, RequestType type, String title)
	 {
		this.langCode = langCode;
		this.type = type;
		this.title = title;
	 }

	
	public String getLangCode() {
		return langCode;
	}

	public RequestType getType() {
		return type;
	}

	public String getTitle() {
		return title;
	}
	
	
}
