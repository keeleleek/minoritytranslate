package ee.translate.keeleleek.mtapplication.model.content;

public class WikiSubReference extends WikiReference {

	private int depth;
	private boolean addArticles;
	private boolean addCategories;
	

	// INIT
	public WikiSubReference(String langCode, Namespace namespace, String wikiTitle, int depth, boolean addArticles, boolean addCategories) throws NullPointerException
	 {
		super(langCode, namespace, wikiTitle);
		
		this.depth = depth;
		
		this.addArticles = addArticles;
		this.addCategories = addCategories;
	 }
	
	public WikiSubReference(String langCode, Namespace namespace, String wikiTitle, boolean addArticles, boolean addCategories) throws NullPointerException
	 {
		this(langCode, namespace, wikiTitle, 0, addArticles, addCategories);
	 }

	public WikiSubReference(String langCode, Namespace namespace, String wikiTitle) throws NullPointerException
	 {
		this(langCode, namespace, wikiTitle, 0, true, false);
	 }

	public WikiSubReference(String langCode, Namespace namespace, String wikiTitle, int depth) throws NullPointerException
	 {
		this(langCode, namespace, wikiTitle, depth, true, false);
	 }

	public WikiSubReference(WikiSubReference other, String title, int depth) throws NullPointerException
	 {
		this(other.getLangCode(), other.getNamespace(), title, depth, other.addArticles, other.addCategories);
	 }
	
	
	// ADDING
	public int getDepth() {
		return depth;
	}
	
	public boolean isAddArticles() {
		return addArticles;
	}
	
	public boolean isAddCategories() {
		return addCategories;
	}
	
	
}
