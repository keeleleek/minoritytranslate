package ee.translate.keeleleek.mtapplication.model.filters;

import java.util.ArrayList;

public class TemplatesFilter implements Filter {
	
	public final static String NAME = "templates";
	
	
	@Override
	public String getName() {
		return NAME;
	}
	
	@Override
	public String filter(String text) {
		
		ArrayList<Integer> hits = new ArrayList<>();
		
		int l = 0;
		boolean ref = false;
		int len = text.length();
		
		hits.add(0);
		
		for (int i = 0; i < len; i++) {
			
			if (i < len - 1) {
				if (text.charAt(i) == '{' && text.charAt(i + 1) == '{') {
					l++;
					if (l == 1 && !ref) hits.add(i);
					i++;
				}
				if (text.charAt(i) == '}' && text.charAt(i + 1) == '}') {
					if (l == 1 && !ref) {
						if (i < len - 2 && text.charAt(i + 2) == '\n') hits.add(i + 3); 
						else hits.add(i + 2);
					}
					l--;
					i++;
				}
			}

			if (text.charAt(i) == '<' && text.charAt(i + 1) == 'r' && text.charAt(i + 2) == 'e' && text.charAt(i + 3) == 'f') ref = true; // <ref
			if (text.charAt(i) == '<' && text.charAt(i + 1) == '/' && text.charAt(i + 2) == 'r' && text.charAt(i + 3) == 'e' && text.charAt(i + 4) == 'f' && text.charAt(i + 5) == '>') ref = false; // </ref>
			if (text.charAt(i) == '/' && text.charAt(i + 1) == '>') ref = false; // />
		}

		hits.add(len);
		
		StringBuffer result = new StringBuffer();
		
		int p = -1;
		boolean template = true;
		for (Integer n : hits) {
			if (!template) result.append(text.substring(p, n));
			template = !template;
			p = n;
		}
		
		return result.toString();
	}
	
}
