package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import ee.translate.keeleleek.mtapplication.view.application.ApplicationMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.ManualDialogMediator;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;


public class ManualDialogMediatorFX extends ManualDialogMediator {

	@FXML
	private WebView programWebView;
	@FXML
	private WebView wikitextWebView;
	
	protected String programHTML = "";
	protected String wikitextHTML = "";
	
	
	// INITIATION:
	public ManualDialogMediatorFX() {
	}
	
	
	// IMPLEMENTATION:
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
		setBrowserLoadBehaviour(programWebView.getEngine(), programHTML);
		setVoidLoadBehaviour(wikitextWebView.getEngine(), wikitextHTML);
	 }

	@Override
	protected void loadProgramPage(String html) {
		this.programHTML = html;
		programWebView.getEngine().loadContent(html);
	}
	
	@Override
	protected void loadWikitextPage(String html) {
		this.wikitextHTML = html;
		wikitextWebView.getEngine().loadContent(html);
	}
	
	
	// UTILITY:
	private void setBrowserLoadBehaviour(final WebEngine engine, final String content) {
		engine.locationProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> ov, final String oldLoc, final String loc) {
				ApplicationMediator mediator = (ApplicationMediator) getFacade().retrieveMediator(ApplicationMediator.NAME);
				mediator.showDocument(loc);
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						engine.loadContent(content);;
					}
				});
			}
		});
	}

	private void setVoidLoadBehaviour(final WebEngine engine, final String content) {
		engine.locationProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> ov, final String oldLoc, final String loc) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						engine.loadContent(content);;
					}
				});
			}
		});
	}
	
}
