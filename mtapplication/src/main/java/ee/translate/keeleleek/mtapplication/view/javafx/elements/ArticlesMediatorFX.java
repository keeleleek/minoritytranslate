package ee.translate.keeleleek.mtapplication.view.javafx.elements;

import java.util.Arrays;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.media.IconsProxyFX;
import ee.translate.keeleleek.mtapplication.view.elements.ArticlesMediator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;


public class ArticlesMediatorFX extends ArticlesMediator {

	private ListView<Label> articleList;
	
	
	// INIT
	public ArticlesMediatorFX(ListView<Label> articleList) {
		this.articleList = articleList;
		
		this.articleList.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>()
		 {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				selectedIndexChanged(newValue.intValue());
			}
		 });
	}

	
	// INHERIT
	@Override
	protected void setList(String[] items)
	 {
		IconsProxyFX icons = (IconsProxyFX) MinorityTranslateModel.icons();
		Label[] labels = new Label[items.length];
		for (int i = 0; i < items.length; i++) {
			
			ImageView image = icons.getArticonImage(MinorityTranslateModel.content().findArticlon(i));
			labels[i] = new Label(items[i], image);
			
		}
		
		articleList.setItems(FXCollections.observableList(Arrays.asList(labels)));
	 }
	
	@Override
	protected void fetchArticon(int i)
	 {
		if (i < 0 || i >= articleList.getItems().size()) return;
		
		IconsProxyFX icons = (IconsProxyFX) MinorityTranslateModel.icons();
		ImageView image = icons.getArticonImage(MinorityTranslateModel.content().findArticlon(i));
		articleList.getItems().get(i).setGraphic(image);
	 }
	
	@Override
	protected void setSelectedIndex(int i)
	 {
		articleList.getSelectionModel().select(i);
		articleList.getFocusModel().focus(i);
//		articleList.scrollTo(i);
	 }
	
	
}
