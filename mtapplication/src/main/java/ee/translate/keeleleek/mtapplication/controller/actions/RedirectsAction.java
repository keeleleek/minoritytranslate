package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.Collection;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class RedirectsAction extends MWAction {
	
	public static int MAX_TITLE_COUNT = 50;
	public static int MAX_LANGUAGE_COUNT = 50;
	
	
	private HttpAction action;
	
	private Collection<String> titles;
	private HashMap<String,String> normalisations = new HashMap<>();
	private HashMap<String,String> redirects = new HashMap<>();
	
	
	// INIT
	public RedirectsAction(Collection<String> titles)
	 {
		if (titles.size() > MAX_TITLE_COUNT) throw new IllegalArgumentException("titles count exceeds " + MAX_TITLE_COUNT);
		
		this.titles = titles;
		
		action = getAction();
	 }
	
	
	// ACTION
	public HttpAction getAction()
	 {
		StringBuffer strTitles = new StringBuffer();
		for (String title : titles) {
			if(strTitles.length() > 0) strTitles.append("|");
			strTitles.append(title);
		}
		
		HttpAction action = new RequestBuilder(MediaWiki.URL_API)
		.param("action", "query")
		.param("titles", MediaWiki.urlEncode(strTitles.toString()))
		.param("redirects", "yes")
		.param("format", "xml")
		.buildGet();
		
		return action;
	 }

	@Override
	public HttpAction getNextMessage() {
		HttpAction action = this.action;
		this.action = null;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return action != null;
	}

	
	// PROCESS
	@Override
	public String processReturningText(String response, HttpAction action)
	 {
//		System.out.println("RESPONSE=" + response);
		
		Pattern pFrom = Pattern.compile("from=\"(.*?)\"");
		Pattern pTo = Pattern.compile("to=\"(.*?)\"");
		Pattern pnEntry = Pattern.compile(Pattern.quote("<n ") + "(.*?)" + Pattern.quote(" />"));
		Pattern prEntry = Pattern.compile(Pattern.quote("<r ") + "(.*?)" + Pattern.quote(" />"));
		
		Matcher mnEntry = pnEntry.matcher(response);
		while (mnEntry.find()) {
			
			String nEntry = mnEntry.group(0);
			
			Matcher mFrom = pFrom.matcher(nEntry);
			Matcher mTo = pTo.matcher(nEntry);
			
			if (!mFrom.find() || !mTo.find()) continue;
			
			String from = mFrom.group(1);
			String to = mTo.group(1);
			
			normalisations.put(from, to);
			
		}
		
		Matcher mrEntry = prEntry.matcher(response);
		while (mrEntry.find()) {
			String rEntry = mrEntry.group(0);
			
			Matcher mFrom = pFrom.matcher(rEntry);
			Matcher mTo = pTo.matcher(rEntry);
			
			if (!mFrom.find() || !mTo.find()) continue;
			
			String from = mFrom.group(1);
			String to = mTo.group(1);
			
			redirects.put(from, to);
			
		}
		
		return super.processReturningText(response, action);
	 }
	
	
	// RESULT
	public String findRedirect(String from)
	 {
		String to;
		
		to = normalisations.get(from);
		if (to != null) from = to;

		to = redirects.get(from);
		if (to != null) from = to;
		
		return from;
	 }
	
}
