package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import java.util.ArrayList;

import ee.translate.keeleleek.mtapplication.model.preferences.CollectEntry;
import ee.translate.keeleleek.mtapplication.model.preferences.CollectPreferences;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.CollectFX;
import ee.translate.keeleleek.mtapplication.view.pages.CollectPageMediator;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.Callback;


public class CollectPageMediatorFX extends CollectPageMediator {

	@FXML
	private TableView<CollectFX> valuesTable;
	@FXML
	private TableColumn<CollectFX, String> findColumn;
	@FXML
	private TableColumn<CollectFX, String> filterColumn;

	@FXML
	private Button insertButton;
	@FXML
	private Button appendButton;
	@FXML
	private Button moveUpButton;
	@FXML
	private Button moveDownButton;
	@FXML
	private Button removeButton;

	@FXML
	private TextField srcLangEdit;
	@FXML
	private TextField dstLangEdit;
	@FXML
	private TextArea findEdit;
	
	
	// IMPLEMENT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		findColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<CollectFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<CollectFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(mapping.getValue().find);
				return prop;
			}
		});

		filterColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<CollectFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<CollectFX, String> mapping) {
				StringProperty prop = new SimpleStringProperty();
				prop.bind(Bindings.concat(mapping.getValue().srcLang, " \u2192 ", mapping.getValue().dstLang));
				return prop;
			}
		});
		
		valuesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<CollectFX>() {
			@Override
			public void changed(ObservableValue<? extends CollectFX> obs, CollectFX oldVal, CollectFX newVal)
			 {
				if (oldVal != null) {
					oldVal.srcLang.unbind();
					oldVal.dstLang.unbind();
					oldVal.find.unbind();
				}
				if (newVal != null) {

					srcLangEdit.setText(newVal.srcLang.getValue());
					newVal.srcLang.bind(srcLangEdit.textProperty());

					dstLangEdit.setText(newVal.dstLang.getValue());
					newVal.dstLang.bind(dstLangEdit.textProperty());

					findEdit.setText(newVal.find.getValue());
					newVal.find.bind(findEdit.textProperty());
					
				}
			 }
		});
	
		valuesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<CollectFX>() {
			@Override
			public void changed(ObservableValue<? extends CollectFX> obs, CollectFX oldVal, CollectFX newVal)
			 {
				appendButton.setDisable(false);
				insertButton.setDisable(false);
				moveUpButton.setDisable(newVal == null);
				moveUpButton.setDisable(newVal == null);
				moveDownButton.setDisable(newVal == null);
				removeButton.setDisable(newVal == null);
				srcLangEdit.setDisable(newVal == null);
				dstLangEdit.setDisable(newVal == null);
				findEdit.setDisable(newVal == null);
				if (newVal == null) {
					srcLangEdit.setText("");
					dstLangEdit.setText("");
					findEdit.setText("");
				}
			 }
		});
		
	 }

	
	// PAGE
	@Override
	public void open(CollectPreferences preferences)
	 {
		ObservableList<CollectFX> mappingListFX = FXCollections.observableArrayList();
		for (int i = 0; i < preferences.getCollectCount(); i++) {
			mappingListFX.add(new CollectFX(preferences.getCollect(i)));
		}
		
		valuesTable.setItems(mappingListFX);

		srcLangEdit.setText("");
		dstLangEdit.setText("");
		findEdit.setText("");
		
		valuesTable.getSelectionModel().selectFirst();
	 }
	
	@Override
	public CollectPreferences close()
	 {
		ObservableList<CollectFX> mappingListFX = valuesTable.getItems();
		ArrayList<CollectEntry> mappingList = new ArrayList<>(mappingListFX.size());
		for (CollectFX mappingFX : mappingListFX) {
			mappingList.add(mappingFX.toEntry());
		}
		
		return new CollectPreferences(mappingList);
	 }
	
	
	// TABLE
	private void addRow(int index)
	 {
		ObservableList<CollectFX> items = valuesTable.getItems();
		if (index > items.size()) index = items.size();
		if (index < 0) index = 0;
		
		items.add(index, new CollectFX(new CollectEntry()));
		valuesTable.getSelectionModel().select(items.get(index));
	 }
	
	private void removeRow(int index)
	 {
		ObservableList<CollectFX> items = valuesTable.getItems();
		if (index >= items.size()) return;
		if (index < 0) return;

		boolean select = valuesTable.getSelectionModel().getSelectedIndex() != -1;
		
		items.remove(index);
		
		if (select) {
			if (index == items.size()) index--;
			valuesTable.getSelectionModel().select(index);
		}
	 }

	private void moveRowDown(int index)
	 {
		ObservableList<CollectFX> items = valuesTable.getItems();
		if (index >= items.size() - 1) return;
		if (index < 0) return;

		boolean select = valuesTable.getSelectionModel().getSelectedIndex() != -1;
		
		CollectFX item = items.remove(index);
		items.add(index + 1, item);

		if (select) {
			valuesTable.getSelectionModel().select(item);
		}
	 }

	private void moveRowUp(int index)
	 {
		ObservableList<CollectFX> items = valuesTable.getItems();
		if (index >= items.size()) return;
		if (index < 1) return;

		boolean select = valuesTable.getSelectionModel().getSelectedIndex() != -1;
		
		CollectFX item = items.remove(index);
		items.add(index - 1, item);

		if (select) {
			valuesTable.getSelectionModel().select(item);
		}
	 }
	
	
	// ACTIONS
	@FXML
	private void onAppendClick() {
		addRow(valuesTable.getSelectionModel().getSelectedIndex() + 1);
	}
	
	@FXML
	private void onInsertClick() {
		addRow(valuesTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveUpClick() {
		moveRowUp(valuesTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onMoveDownClick() {
		moveRowDown(valuesTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void onRemoveClick() {
		removeRow(valuesTable.getSelectionModel().getSelectedIndex());
	}

	
}
