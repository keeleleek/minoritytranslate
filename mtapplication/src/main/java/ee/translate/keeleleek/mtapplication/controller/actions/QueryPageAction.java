package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class QueryPageAction extends MWAction {
	
	public static final int LIMIT = 50;
	
	public static final String MOST_LINKED_LIST = "Mostlinked";
	public static final String MOST_INTERWIKIS_LIST = "Mostinterwikis";
	public static final String MOST_CATEGORIES_LIST = "Mostcategories";
	public static final String LONG_PAGES_LIST = "Longpages";
	
	private String qpoffset;
	private String list;
	private List<String> results = new ArrayList<>();
	
	private HttpAction action;
	private boolean popped = false;
	
	
	
	public QueryPageAction(String list, String qpoffset) {
		this.list = list;
		this.qpoffset = qpoffset;
		action = getAction();
	}
	
	public QueryPageAction(String list) {
		this(list, null);
	}
	
	public QueryPageAction next() {
		if (qpoffset == null) return null;
		return new QueryPageAction(list, qpoffset);
	}
	
	
	public HttpAction getAction()
	 {
		RequestBuilder builder = new RequestBuilder(MediaWiki.URL_API)
		.param("action", "query")
		.param("list", "querypage")
		.param("qppage", list)
		.param("qplimit", LIMIT)
		.param("format", "xml");
		
		if (qpoffset != null) builder.param("qpoffset", qpoffset);
		
		return builder.buildGet();
	 }
	
	@Override
	public HttpAction getNextMessage() {
		popped = true;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return !popped;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		Pattern p3 = Pattern.compile("qpoffset=\"(.*?)\"");
		Matcher m3 = p3.matcher(response);
		if (m3.find()) qpoffset = m3.group(1);
		else qpoffset = null;
		
		Pattern p0 = Pattern.compile("<page (.*?)\\>");
		Matcher m0 = p0.matcher(response);

		while (m0.find()) {
			String page = m0.group(1);

			Pattern p1 = Pattern.compile("title=\"(.*?)\"");
			Matcher m1 = p1.matcher(page);
			
			Pattern p2 = Pattern.compile("ns=\"(.*?)\"");
			Matcher m2 = p2.matcher(page);
			
			m2.find();
			String ns = m2.group(1);
			
			if (!ns.equals("0")) continue;
			
			m1.find();
			results.add(m1.group(1));
		}
		
		return super.processReturningText(response, action);
	 }
	

	public List<String> getResult() {
		return results;
	}
	
	public String getQpoffset() {
		return qpoffset;
	}
	
}
