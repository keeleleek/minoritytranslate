package ee.translate.keeleleek.mtapplication.model.media;

import java.util.HashMap;

import javax.swing.ImageIcon;

import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Articon;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.Status;

public class IconsProxySwing extends IconsProxy {

	
	private HashMap<Status, ImageIcon> icons = new HashMap<>();
	private HashMap<Articon, ImageIcon> articons = new HashMap<>();
	
	
	// INIT
	public IconsProxySwing()
	 {
		icons.put(Status.UPLOAD_CONFIRMING, new ImageIcon(IconsProxy.class.getResource("/icons/UPLOAD_CONFIRMING.png")));
		icons.put(Status.UNKNOWN, new ImageIcon(IconsProxy.class.getResource("/icons/UNKNOWN.png")));
		icons.put(Status.UPLOAD_CONFLICT, new ImageIcon(IconsProxy.class.getResource("/icons/UPLOAD_CONFLICT.png")));
		icons.put(Status.UPLOAD_FAILED, new ImageIcon(IconsProxy.class.getResource("/icons/UPLOAD_FAILED.png")));
//		icons.put(Status.STANDBY, new ImageIcon(IconsProxy.class.getResource("/icons/STANDBY.png")));
		icons.put(Status.DOWNLOAD_REQUESTED, new ImageIcon(IconsProxy.class.getResource("/icons/DOWNLOAD_REQUESTED.png")));
		icons.put(Status.DOWNLOAD_IN_PROGRESS, new ImageIcon(IconsProxy.class.getResource("/icons/DOWNLOADING.png")));
		icons.put(Status.UPLOAD_REQUESTED, new ImageIcon(IconsProxy.class.getResource("/icons/UPLOAD_REQUESTED.png")));
		icons.put(Status.UPLOAD_PREPARING, new ImageIcon(IconsProxy.class.getResource("/icons/PREPARING_UPLOAD.png")));
		icons.put(Status.UPLOAD_IN_PROGRESS, new ImageIcon(IconsProxy.class.getResource("/icons/UPLOADING.png")));

		articons.put(Articon.UNKNOWN, new ImageIcon(IconsProxy.class.getResource("/icons/MT-icon-UNKNOWN.png")));
		articons.put(Articon.EXISTS, new ImageIcon(IconsProxy.class.getResource("/icons/MT-icon-EXISTS.png")));
		articons.put(Articon.NEW, new ImageIcon(IconsProxy.class.getResource("/icons/MT-icon-NEW.png")));
		articons.put(Articon.EDITED, new ImageIcon(IconsProxy.class.getResource("/icons/MT-icon-EDITED.png")));
		articons.put(Articon.BUSY, new ImageIcon(IconsProxy.class.getResource("/icons/MT-icon-BUSY.png")));
		articons.put(Articon.PROBLEM, new ImageIcon(IconsProxy.class.getResource("/icons/MT-icon-PROBLEM.png")));
	 }

	
	// INHERIT (ICONS)
	@Override
	public ImageIcon getStatusIcon(Status status) {
		return icons.get(status);
	}
	
	@Override
	public ImageIcon getArticon(Articon articon) {
		return articons.get(articon);
	}
	
	
}
