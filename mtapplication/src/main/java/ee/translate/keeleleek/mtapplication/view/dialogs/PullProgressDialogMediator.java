package ee.translate.keeleleek.mtapplication.view.dialogs;

import org.puremvc.java.multicore.patterns.mediator.Mediator;


public abstract class PullProgressDialogMediator extends Mediator {

	public final static String NAME = "{5B7FCC8D-CC91-44C6-9876-9ACB7AAC9742}";
	
	
	// INIT
	public PullProgressDialogMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}
	
	
	// INHERIT (PROGRESS)
	public abstract void setCurrent(int current);
	public abstract void setTotal(int total);
	public abstract void setAction(String action);

	
}
