package ee.translate.keeleleek.mtapplication.controller.lists;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class ResetSuggestionLists extends SimpleCommand {

	
	@Override
	public void execute(INotification notification)
	 {
		MinorityTranslateModel.lists().reset();
	 }
	
}
