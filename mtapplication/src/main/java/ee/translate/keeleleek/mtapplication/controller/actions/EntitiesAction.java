package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class EntitiesAction extends MWAction {
	
	public static int MAX_TITLE_COUNT = 50;
	public static int MAX_LANGUAGE_COUNT = 50;
	
	
	private HttpAction action;
	
	private String langCode;
	private Collection<String> titles;
	private Collection<String> langFilter;
	
	private HashMap<String, String> qids = new HashMap<>();
	private HashMap<String, HashMap<String, String>> langTitles = new HashMap<>();
	
	public EntitiesAction(String langCode, Collection<String> titles, Collection<String> langFilter)
	 {
		if (titles.size() > MAX_TITLE_COUNT) throw new IllegalArgumentException("titles count exceeds " + MAX_TITLE_COUNT);
		
		this.langCode = langCode;
		this.titles = titles;
		this.langFilter = langFilter;
		
		action = createAction();
	 }
	
	public EntitiesAction(String langCode, Collection<String> titles, String langFilter)
	 {
		this(langCode, titles, toList(langFilter));
	 }
	
	public HttpAction createAction()
	 {
		StringBuffer strTitles = new StringBuffer();
		for (String title : titles) {
			if(strTitles.length() > 0) strTitles.append("|");
			strTitles.append(title);
		}
		
		StringBuffer strFilter = new StringBuffer();
		for (String langCode : langFilter) {
			if(strFilter.length() > 0) strFilter.append("|");
			strFilter.append(langCode.replace('-', '_') + "wiki");
		}
		
		HttpAction action = new RequestBuilder(MediaWiki.URL_API)
		.param("action", "wbgetentities")
		.param("titles", MediaWiki.urlEncode(strTitles.toString()))
		.param("sites", langCode.replace('-', '_') + "wiki")
		.param("props", "sitelinks")
//		.param("sitefilter", MediaWiki.urlEncode(strFilter.toString()))
		.param("format", "xml")
		.buildGet();
		
		return action;
	 }

	@Override
	public HttpAction getNextMessage() {
		HttpAction popped = this.action;
		this.action = null;
		return popped;
	}

	@Override
	public boolean hasMoreMessages() {
		return action != null;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		//System.out.println("RESPONSE = " + response);
		
		//Pattern pEntity = Pattern.compile("<Q(?:.+?)>" + "(.+?)" + "</Q(?:.+?)>");
		Pattern pEntity = Pattern.compile(Pattern.quote("<entity ") + "(.+?)" + Pattern.quote("</entity>"));
		Pattern pQid = Pattern.compile(Pattern.quote("id=\"") + "(.+?)" + Pattern.quote("\""));
		Pattern pSitelink = Pattern.compile(Pattern.quote("<sitelink ") + "(.+?)" + Pattern.quote(">"));
		Pattern pSite = Pattern.compile(Pattern.quote("site=\"") + "(.+?)" + Pattern.quote("\""));
		Pattern pTitle = Pattern.compile(Pattern.quote("title=\"") + "(.+?)" + Pattern.quote("\""));
		
		Matcher mEntity = pEntity.matcher(response);
		while (mEntity.find()) {
			
			String entity = mEntity.group(0);
			
			Matcher mQid = pQid.matcher(entity);
			if (!mQid.find()) continue;
			String qid = mQid.group(1);
			
			Matcher mSitelink = pSitelink.matcher(entity);
			while (mSitelink.find()) {
				
				String sitelink = mSitelink.group(0);
				
				Matcher mSite = pSite.matcher(sitelink);
				Matcher mTitle = pTitle.matcher(sitelink);
				
				if (!mSite.find() || !mTitle.find()) continue;
				
				String langCode = mSite.group(1).replace("wiki", "").replace('_', '-');
				String title = MediaWiki.urlDecode(mTitle.group(1));
				
				if (langCode.equals(this.langCode)) qids.put(title, qid);
				
				HashMap<String, String> titleslang = langTitles.get(qid);
				if (titleslang == null) {
					titleslang = new HashMap<>();
					langTitles.put(qid, titleslang);
				}
				titleslang.put(langCode, title);
				
			}
			
		}
		
		return super.processReturningText(response, action);
	 }
	
	
	// RESULT
	public String findQid(String title) {
		return qids.get(title);
	}

	public HashMap<String, String> findLangtitles(String qid) {
		return langTitles.get(qid);
	}

	public String findLangtitle(String qid, String langCode) {
		if (langTitles.containsKey(qid)) return langTitles.get(qid).get(langCode);
		return null;
	}
	
	// HELPERS
	private static List<String> toList(String element)
	 {
		List<String> result = new ArrayList<>(1);
		result.set(0, element);
		return result;
	 }
	
}
