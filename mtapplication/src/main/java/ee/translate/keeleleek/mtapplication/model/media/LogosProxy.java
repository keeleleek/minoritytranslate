package ee.translate.keeleleek.mtapplication.model.media;

import org.puremvc.java.multicore.patterns.proxy.Proxy;

public abstract class LogosProxy extends Proxy {

	public final static String NAME = "{EE1EACEC-FFEA-4C1B-91DE-778E3CE133C9}";
	
	
	// INITIATION:
	public LogosProxy() {
		super(NAME, null);
	}

	
	/**
	 * Gets application logo. Type depends on implementation.
	 * 
	 * @return application logo
	 */
	public abstract Object retrieveApplicationLogo();

	/**
	 * Gets Keeleleek logo. Type depends on implementation.
	 * 
	 * @return Keeleleek logo
	 */
	public abstract Object retrieveKeeleleekLogo();

	/**
	 * Gets Wikimedia logo. Type depends on implementation.
	 * 
	 * @return Wikimedia logo
	 */
	public abstract Object retrieveWMFLogo();
	
}
