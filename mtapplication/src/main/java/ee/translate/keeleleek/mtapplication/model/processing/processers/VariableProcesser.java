package ee.translate.keeleleek.mtapplication.model.processing.processers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.PatternSyntaxException;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.Namespace;
import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class VariableProcesser {

	private final static String TITLE_VARIABLE_LOWERCASE = "${title}";
	private final static String TITLE_VARIABLE_UPPERCASE = "${Title}";


	public static String process(String langCode, String text) throws PatternSyntaxException
	 {
		text = namespaceProcess(text, langCode);
		
		String qid = MinorityTranslateModel.content().getSelectedQid();
		Reference ref = null;
		MinorityArticle article = null;
		String title = null;
		if (qid != null) ref = new Reference(qid, langCode);
		if (ref != null) article = MinorityTranslateModel.content().getArticle(ref);
		if (article!= null) title = article.getTitle();
		
		return process(langCode, title, text, null);
	 }
	
	public static String process(String langCode, String title, String text, HashMap<String, String> variableMap) throws PatternSyntaxException
	 {
		// variable map
		if (variableMap != null) {

			Set<Entry<String, String>> entries = variableMap.entrySet();

			for (Entry<String, String> entry : entries) {
				text = text.replace("${" + entry.getKey() + "}", entry.getValue());
			}

		}

		// title
		if (title != null) {
			text = titleProcess(text, title);
		}
		
		// namespaces
		text = namespaceProcess(text, langCode);
		
		return text;
	 }
	
	public static List<String> collectAllVariables() {
		ArrayList<String> variables = new ArrayList<>();

		variables.add(TITLE_VARIABLE_LOWERCASE);
		variables.add(TITLE_VARIABLE_UPPERCASE);
		
		Namespace[] namespaces = Namespace.values();
		for (Namespace namespace : namespaces) {
			if (namespace.isEmpty()) continue;
			variables.add(namespace.getVariableName());
		}
		
		return variables;
	}
	


	private static String namespaceProcess(String text, String langCode)
	 {
		Namespace[] namespaces = Namespace.values();
		for (Namespace namespace : namespaces) {
			
			String nsName = MinorityTranslateModel.wikis().findNamespace(langCode, namespace);
			if (nsName == null || nsName.isEmpty()) continue;
			
			text = text.replace(namespace.getVariableName(), nsName);
			
		}
		
		return text;
	 }
	
	private static String titleProcess(String text, String title)
	 {
		if (title.isEmpty()) return text;

		String utitle = title.substring(0, 1).toUpperCase() + title.substring(1);
		String ltitle = title.substring(0, 1).toLowerCase() + title.substring(1);

		text = text.replace(TITLE_VARIABLE_LOWERCASE, ltitle);
		text = text.replace(TITLE_VARIABLE_UPPERCASE, utitle);
		
		return text;
	 }
	
	
	
}
