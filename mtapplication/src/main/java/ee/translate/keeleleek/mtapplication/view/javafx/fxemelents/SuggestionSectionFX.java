package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SuggestionSectionFX implements SuggestionFX {

	private StringProperty title;
	
	public SuggestionSectionFX(String title)
	 {
		this.title = new SimpleStringProperty(adjustTitle(title));
	 }
	
	public StringProperty title() {
		return title;
	}

	public StringProperty translated(String langCode) {
		return new SimpleStringProperty("");
	}


	private String adjustTitle(String string) {
		string = trimBold(string);
		return string;
	}
	
	private String trimBold(String string) {
		while (string.endsWith("\'") && string.startsWith("\'")) {
			string = string.substring(1, string.length() - 1);
		}
		return string;
	}

	
}
