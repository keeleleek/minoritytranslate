package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PluginFX {

	public final StringProperty name;
	public final StringProperty version;
	
	
	public PluginFX(String name, String version)
	 {
		this.name= new SimpleStringProperty(name);
		this.version = new SimpleStringProperty(version);
	 }

	
}
