package ee.translate.keeleleek.mtapplication.model.preferences;

/**
 * Immutable wrapper for translate related preferences.
 *
 */
public class TranslatePreferences {

	private LanguagesPreferences languages;
	private CorpusPreferences corpus;
	
	
	public TranslatePreferences(LanguagesPreferences languages, CorpusPreferences corpus) {
		super();
		this.languages = languages;
		this.corpus = corpus;
	}
	
	
	public LanguagesPreferences languages() {
		return languages;
	}
	
	public CorpusPreferences corpus() {
		return corpus;
	}
	
	
}
