package ee.translate.keeleleek.mtapplication.controller.timer;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class TimerStartCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		MinorityTranslateModel.timer().start();
	 }
	
}
