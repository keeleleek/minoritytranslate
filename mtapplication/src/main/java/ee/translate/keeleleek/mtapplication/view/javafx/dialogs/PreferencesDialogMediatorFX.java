package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.ContentAssistPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.InsertsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.LookupsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.SymbolsPreferences;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMappingPreferences;
import ee.translate.keeleleek.mtapplication.view.dialogs.PreferencesDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.MediatorLoaderFX;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.StackPane;


public class PreferencesDialogMediatorFX extends PreferencesDialogMediator {

	@FXML
	private Node rootNode;
	@FXML
	private SplitPane treeContentSplit;
	@FXML
	private TreeView<String> preferencesTree;
	private TreeItem<String> translateItem;
	private TreeItem<String> displayItem;
	private TreeItem<String> contentItem;
	private TreeItem<String> collectItem;
	private TreeItem<String> replaceItem;
	private TreeItem<String> templateMappingItem;
	private TreeItem<String> contentAssistItem;
	private TreeItem<String> insertsItem;
	private TreeItem<String> symbolsItem;
	private TreeItem<String> pullingItem;
	private TreeItem<String> spellingItem;
	private TreeItem<String> lookupsItem;
	private TreeItem<String> lastItem = null;
	
	@FXML
	private StackPane pages;

	private Node translatePage = null;
	private Node displayPage = null;
	private Node contentPage = null;
	private Node pullingPage = null;
	private Node spellingPage = null;
	private Node collectPage = null;
	private Node replacePage = null;
	private Node templateMappingPage = null;
	private Node contentAssistPage = null;
	private Node insertsPage = null;
	private Node symbolsPage = null;
	private Node lookupsPage = null;
	
	
	// INIT
	public PreferencesDialogMediatorFX() {
		super();
	}
	
	@Override
	public void onRegister()
	 {
		super.onRegister();

		// tree
		TreeItem<String> rootItem = new TreeItem<String>("Root");
		preferencesTree.setRoot(rootItem);
		preferencesTree.setShowRoot(false);

		preferencesTree.getSelectionModel().selectedItemProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				show(preferencesTree.getSelectionModel().selectedItemProperty().getValue());
			}
		});

		// translate
		translateItem = new TreeItem<String>(Messages.getString("preferences.translation"));
		rootItem.getChildren().add(translateItem);

		// display
		displayItem = new TreeItem<String>(Messages.getString("preferences.display"));
		rootItem.getChildren().add(displayItem);

		// content
		contentItem = new TreeItem<String>(Messages.getString("preferences.content"));
		rootItem.getChildren().add(contentItem);

		// content assist
		contentAssistItem = new TreeItem<String>(Messages.getString("preferences.content.assist"));
		contentAssistItem.setExpanded(true);
		rootItem.getChildren().add(contentAssistItem);

		//  inserts
		insertsItem = new TreeItem<String>(Messages.getString("preferences.content.assist.inserts"));
		contentAssistItem.getChildren().add(insertsItem);

		//  collect
		collectItem = new TreeItem<String>(Messages.getString("preferences.processing.collect.page"));
		contentAssistItem.getChildren().add(collectItem);

		//  symbols
		symbolsItem = new TreeItem<String>(Messages.getString("preferences.content.assist.symbols"));
		contentAssistItem.getChildren().add(symbolsItem);
		
		// pulling
		pullingItem = new TreeItem<String>(Messages.getString("preferences.pulling"));
		pullingItem.setExpanded(true);
		rootItem.getChildren().add(pullingItem);

		//  template mapping
		templateMappingItem = new TreeItem<String>(Messages.getString("preferences.processing.template.mapping.page"));
		pullingItem.getChildren().add(templateMappingItem);

		//  replace
		replaceItem = new TreeItem<String>(Messages.getString("preferences.processing.replace.page"));
		pullingItem.getChildren().add(replaceItem);

		// spelling
		spellingItem = new TreeItem<String>(Messages.getString("preferences.spelling"));
		spellingItem.setExpanded(true);
		rootItem.getChildren().add(spellingItem);

		// lookups
		lookupsItem = new TreeItem<String>(Messages.getString("preferences.lookups"));
		lookupsItem.setExpanded(true);
		rootItem.getChildren().add(lookupsItem);

		// select
		preferencesTree.getSelectionModel().select(0);
		Platform.runLater(new Runnable() {
			@Override
			public void run()
			 {
				preferencesTree.getFocusModel().focus(0);
			 }
		});
	 }

	@FXML
	private void initialize()
	 {
		
	 }
	
	
	// PREPARE
	@Override
	public void prepare()
	 {
		if (lastItem != null) show(lastItem);
	 }
	
	
	// INHERIT (INIT)
	@Override
	protected void openTemplateMapping(TemplateMappingPreferences preferences)
	 {
		if (templateMappingMediator == null) {
			templateMappingMediator = MediatorLoaderFX.loadTemplateMappingPage();
			getFacade().registerMediator(templateMappingMediator);
		}
		
		if (templateMappingPage == null) {
			templateMappingPage = (Node) templateMappingMediator.getViewComponent();
			pages.getChildren().add(templateMappingPage);
		}
		
		templateMappingMediator.open(preferences);
	 }
	
	@Override
	protected TemplateMappingPreferences closeTemplateMapping()
	 {
		TemplateMappingPreferences preferences = null;
		
		if (templateMappingPage != null) {
			pages.getChildren().remove(templateMappingPage);
			templateMappingPage = null;
		}
		
		if (templateMappingMediator != null) {
			getFacade().removeMediator(templateMappingMediator.getMediatorName());
			preferences = templateMappingMediator.close();
			templateMappingMediator = null;
		}
		
		return preferences;
	 }
	
	@Override
	protected void openContentAssist(ContentAssistPreferences preferences)
	 {
		if (contentAssistMediator == null) {
			contentAssistMediator = MediatorLoaderFX.loadContentAssistPage();
			getFacade().registerMediator(contentAssistMediator);
		}
		
		if (contentAssistPage == null) {
			contentAssistPage = (Node) contentAssistMediator.getViewComponent();
			pages.getChildren().add(contentAssistPage);
		}
		
		contentAssistMediator.open(preferences);
	 }
	
	@Override
	protected ContentAssistPreferences closeContentAssist()
	 {
		ContentAssistPreferences preferences = null;
		
		if (contentAssistPage != null) {
			pages.getChildren().remove(contentAssistPage);
			contentAssistPage = null;
		}
		
		if (contentAssistMediator != null) {
			getFacade().removeMediator(contentAssistMediator.getMediatorName());
			preferences = contentAssistMediator.close();
			contentAssistMediator = null;
		}
		
		return preferences;
	 }
	

	@Override
	protected void openInserts(InsertsPreferences preferences)
	 {
		if (templatesMediator == null) {
			templatesMediator = MediatorLoaderFX.loadInsertsPage();
			getFacade().registerMediator(templatesMediator);
		}
		
		if (insertsPage == null) {
			insertsPage = (Node) templatesMediator.getViewComponent();
			pages.getChildren().add(insertsPage);
		}
		
		templatesMediator.open(preferences);
	 }
	
	@Override
	protected InsertsPreferences closeInserts()
	 {
		InsertsPreferences preferences = null;
		
		if (insertsPage != null) {
			pages.getChildren().remove(insertsPage);
			insertsPage = null;
		}
		
		if (templatesMediator != null) {
			getFacade().removeMediator(templatesMediator.getMediatorName());
			preferences = templatesMediator.close();
			templatesMediator = null;
		}
		
		return preferences;
	 }
	

	@Override
	protected void openSymbols(SymbolsPreferences preferences)
	 {
		if (symbolsMediator == null) {
			symbolsMediator = MediatorLoaderFX.loadSymbolsPage();
			getFacade().registerMediator(symbolsMediator);
		}
		
		if (symbolsPage == null) {
			symbolsPage = (Node) symbolsMediator.getViewComponent();
			pages.getChildren().add(symbolsPage);
		}
		
		symbolsMediator.open(preferences);
	 }
	
	@Override
	protected SymbolsPreferences closeSymbols()
	 {
		SymbolsPreferences preferences = null;
		
		if (symbolsPage != null) {
			pages.getChildren().remove(symbolsPage);
			symbolsPage = null;
		}
		
		if (symbolsMediator != null) {
			getFacade().removeMediator(symbolsMediator.getMediatorName());
			preferences = symbolsMediator.close();
			symbolsMediator = null;
		}
		
		return preferences;
	 }
	

	@Override
	protected void openLookups(LookupsPreferences preferences)
	 {
		if (lookupsMediator == null) {
			lookupsMediator = MediatorLoaderFX.loadLookupsPage();
			getFacade().registerMediator(lookupsMediator);
			lookupsMediator.open(preferences);
		}
		
		if (lookupsPage == null) {
			lookupsPage = (Node) lookupsMediator.getViewComponent();
			pages.getChildren().add(lookupsPage);
		}
	 }
	
	@Override
	protected LookupsPreferences closeLookups()
	 {
		LookupsPreferences preferences = null;
		
		if (lookupsPage != null) {
			pages.getChildren().remove(lookupsPage);
			lookupsPage = null;
		}
		
		if (lookupsMediator != null) {
			getFacade().removeMediator(lookupsMediator.getMediatorName());
			preferences = lookupsMediator.close();
			lookupsMediator = null;
		}
		
		return preferences;
	 }
	
	
	// INHERIT HELPERS
	private void show(TreeItem<String> item)
	 {
		if (item == templateMappingItem) openTemplateMapping(MinorityTranslateModel.preferences().getTemplateMapping());
		if (templateMappingPage != null) templateMappingPage.setVisible(item == templateMappingItem);

		if (item == contentAssistItem) openContentAssist(MinorityTranslateModel.preferences().getContentAssist());
		if (contentAssistPage != null) contentAssistPage.setVisible(item == contentAssistItem);

		if (item == insertsItem) openInserts(MinorityTranslateModel.preferences().getInserts());
		if (insertsPage != null) insertsPage.setVisible(item == insertsItem);

		if (item == symbolsItem) openSymbols(MinorityTranslateModel.preferences().getSymbols());
		if (symbolsPage != null) symbolsPage.setVisible(item == symbolsItem);

		if (item == lookupsItem) openLookups(MinorityTranslateModel.preferences().getLookups());
		if (lookupsPage != null) lookupsPage.setVisible(item == lookupsItem);

		// translate
		if (item == translateItem) {
			if (translatePageMediator == null) { // create mediator
				translatePageMediator = MediatorLoaderFX.loadTranslatePage();
				translatePageMediator.open(MinorityTranslateModel.preferences().translate());
			}
			
			translatePage = (Node) translatePageMediator.getViewComponent(); // add page
			if (!pages.getChildren().contains(translatePage)) {
				pages.getChildren().add(translatePage);
			}
		} else {
			if (translatePage != null) { // remove page
				pages.getChildren().remove(translatePage);
				translatePage = null;
			}
		}

		// display
		if (item == displayItem) {
			if (displayPageMediator == null) { // create mediator
				displayPageMediator = MediatorLoaderFX.loadDisplayPage();
				displayPageMediator.open(MinorityTranslateModel.preferences().display());
			}
			
			displayPage = (Node) displayPageMediator.getViewComponent(); // add page
			if (!pages.getChildren().contains(displayPage)) {
				pages.getChildren().add(displayPage);
			}
		} else {
			if (displayPage != null) { // remove page
				pages.getChildren().remove(displayPage);
				displayPage = null;
			}
		}

		// content
		if (item == contentItem) {
			if (contentPageMediator == null) { // create mediator
				contentPageMediator = MediatorLoaderFX.loadContentPage();
				contentPageMediator.open(MinorityTranslateModel.preferences().content());
			}
			
			contentPage = (Node) contentPageMediator.getViewComponent(); // add page
			if (!pages.getChildren().contains(contentPage)) {
				pages.getChildren().add(contentPage);
			}
		} else {
			if (contentPage != null) { // remove page
				pages.getChildren().remove(contentPage);
				contentPage = null;
			}
		}

		// pulling
		if (item == pullingItem) {
			if (pullingPageMediator == null) { // create mediator
				pullingPageMediator = MediatorLoaderFX.loadPullingPage();
				pullingPageMediator.open(MinorityTranslateModel.preferences().pulling());
			}
			
			pullingPage = (Node) pullingPageMediator.getViewComponent(); // add page
			if (!pages.getChildren().contains(pullingPage)) {
				pages.getChildren().add(pullingPage);
			}
		} else {
			if (pullingPage != null) { // remove page
				pages.getChildren().remove(pullingPage);
				pullingPage = null;
			}
		}

		// spelling
		if (item == spellingItem) {
			if (spellingPageMediator == null) { // create mediator
				spellingPageMediator = MediatorLoaderFX.loadSpellingPage();
				spellingPageMediator.open(MinorityTranslateModel.preferences().spelling());
			}
			
			spellingPage = (Node) spellingPageMediator.getViewComponent(); // add page
			if (!pages.getChildren().contains(spellingPage)) {
				pages.getChildren().add(spellingPage);
			}
		} else {
			if (spellingPage != null) { // remove page
				pages.getChildren().remove(spellingPage);
				spellingPage = null;
			}
		}

		// collect
		if (item == collectItem) {
			if (collectPageMediator == null) { // create mediator
				collectPageMediator = MediatorLoaderFX.loadCollectPage();
				collectPageMediator.open(MinorityTranslateModel.preferences().collects());
			}
			
			collectPage = (Node) collectPageMediator.getViewComponent(); // add page
			if (!pages.getChildren().contains(collectPage)) {
				pages.getChildren().add(collectPage);
			}
		} else {
			if (collectPage != null) { // remove page
				pages.getChildren().remove(collectPage);
				collectPage = null;
			}
		}

		// replace
		if (item == replaceItem) {
			if (replacePageMediator == null) { // create mediator
				replacePageMediator = MediatorLoaderFX.loadReplacePage();
				replacePageMediator.open(MinorityTranslateModel.preferences().replaces());
			}
			
			replacePage = (Node) replacePageMediator.getViewComponent(); // add page
			if (!pages.getChildren().contains(replacePage)) {
				pages.getChildren().add(replacePage);
			}
		} else {
			if (replacePage != null) { // remove page
				pages.getChildren().remove(replacePage);
				replacePage = null;
			}
		}
		
		lastItem = item;
	 }
	
	
	// EVENTS (PROGRAM)
	
}
