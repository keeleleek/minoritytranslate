package ee.translate.keeleleek.mtapplication.controller.session;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class ChangeUploadEnabledCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Boolean enabled = (Boolean) notification.getBody();
		
		MinorityTranslateModel.session().setUploadEnabled(enabled);
	 }
	
}
