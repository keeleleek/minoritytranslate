package ee.translate.keeleleek.mtapplication.controller.actions;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.ActionException;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class Redirects extends MWAction {
	
	public static int MAX_TITLE_COUNT = 50;
	public static int MAX_LANGUAGE_COUNT = 50;
	
	
	private HttpAction action;
	
	private List<String> titles;
	
	private ArrayList<String> translated = new ArrayList<>();
	private boolean popped = false;
	
	
	public Redirects(List<String> titles) {
		this.titles = titles;
		Collections.sort(this.titles);
		action = getAction();
		
		if (titles.size() > MAX_TITLE_COUNT) throw new IllegalArgumentException("titles count exceeds " + MAX_TITLE_COUNT);
	}
	
	public HttpAction getAction()
	 {
		StringBuffer strTitles = new StringBuffer();
		for (String title : titles) {
			if(strTitles.length() > 0) strTitles.append("|");
			strTitles.append(title);
		}
		
		String titles = strTitles.toString();
		try {
			titles = URLEncoder.encode(titles,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		HttpAction action = new RequestBuilder(MediaWiki.URL_API)
		.param("action", "query")
		.param("titles", titles)
		.param("redirects", "")
		.param("format", "xml")
		.buildGet();
		
		return action;
	 }
	
	@Override
	public HttpAction getNextMessage() {
		popped = true;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return !popped;
	}

	public ArrayList<String> getTranslated() {
		return translated;
	}
	
	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		if (response.contains("error")) {
			Pattern errFinder = Pattern.compile("<p>(.*?)</p>", Pattern.DOTALL | Pattern.MULTILINE);
			Matcher m = errFinder.matcher(response);
			String lastP = "";
			while (m.find()) {
			  lastP = MediaWiki.urlDecode(m.group(1));
			}

			throw new ActionException("Language link retrieval failed - " + lastP);
		}
		
		Pattern p1 = Pattern.compile("title=\"(.*?)\"");
		Matcher m1 = p1.matcher(response);
		
		while (m1.find()) {
			translated.add(MediaWiki.urlDecode(m1.group(1)));
		}
		
		return super.processReturningText(response, action);
	 }
	
}
