package ee.translate.keeleleek.mtapplication.common;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class LazyCaller implements Runnable {

	private Runnable target;
	boolean guiSafe = false;
	
	long delay;
	
	boolean ready;
	boolean waiting = false;
	
	boolean pending = false;
	
	
	// INIT
	public LazyCaller(Runnable target, long delay)
	 {
		this.target = target;
		this.delay = delay;
	 }
	
	public LazyCaller(Runnable target, long delay, boolean guiSafe)
	 {
		this.target = target;
		this.delay = delay;
		this.guiSafe = guiSafe;
	 }
	
	public void force()
	 {
		if (pending) target.run();
		pending = false;
	 }
	
	public void call()
	 {
		pending = true;
		ready = false;
		
		if (!waiting) {
			Thread thread = new Thread(this);
			thread.start();
		}
	 }
	
	@Override
	public void run()
	 {
		waiting = true;
		
		try {
			while (pending) {
				ready = true;
				Thread.sleep(delay);
				if (ready) break;
			}
		} catch (InterruptedException e) {
			System.err.println("Thread sleep failure!");
		}

		waiting = false;
		
		if (pending) {
			if (guiSafe) MinorityTranslateModel.notifier().doThreadSafe(target);
			else target.run();
		}
		pending = false;
	}
	
	
}
