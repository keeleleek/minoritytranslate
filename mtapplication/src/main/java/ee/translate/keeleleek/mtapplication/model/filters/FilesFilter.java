package ee.translate.keeleleek.mtapplication.model.filters;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilesFilter implements Filter {
	
	public final static String NAME = "files";
	
	
	@Override
	public String getName() {
		return NAME;
	}
	
	@Override
	public String filter(String text) {
		
		ArrayList<Integer> hits = new ArrayList<>();
		
		int l = 0;
		int len = text.length();
		
		int s = -1;
		int e = -1;
		
		String regex = Pattern.quote("[[") + "(.*?)" + Pattern.quote(":") + "(.*?)" + Pattern.quote(".") + "(.*?)" + Pattern.quote("|") + "(.*?)" + Pattern.quote("]]");
		
		hits.add(0);
		
		for (int i = 0; i < len; i++) {
			
			
			if (i < len - 1) {
				if (text.charAt(i) == '[' && text.charAt(i + 1) == '[') {
					l++;
					if (l == 1) s = i;
					i++;
				}
				if (text.charAt(i) == ']' && text.charAt(i + 1) == ']') {
					if (l == 1) {
						if (i < len - 2 && (text.charAt(i + 2) == '\n' || text.charAt(i + 2) == ' ')) e = i + 3; 
						else e = i + 2;
					}
					l--;
					i++;
				}
			}

			if (l == 0 && s != -1 && e != -1) {
				String sub = text.substring(s, e);
				Pattern p1 = Pattern.compile(regex, Pattern.DOTALL);
				Matcher m1 = p1.matcher(sub);
				if (m1.find()) {
					hits.add(s);
					hits.add(e);
				}
				s = -1;
				e = -1;
			}
			
		}

		hits.add(len);
		
		StringBuffer result = new StringBuffer();
		
		int p = -1;
		boolean template = true;
		for (Integer n : hits) {
			if (!template) result.append(text.substring(p, n));
			template = !template;
			p = n;
		}
		
		return result.toString();
	}
	
}
