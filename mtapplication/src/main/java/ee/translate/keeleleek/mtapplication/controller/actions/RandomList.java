package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class RandomList extends MWAction {
	
	public static final int LIMIT = 10;
	
	private List<String> results = new ArrayList<>();
	
	private HttpAction action;
	private boolean popped = false;
	
	
	
	public RandomList() {
		action = getAction();
	}
	
	public HttpAction getAction()
	 {
		RequestBuilder builder = new RequestBuilder(MediaWiki.URL_API)
		.param("action", "query")
		.param("list", "random")
		.param("rnnamespace", 0)
		.param("rnlimit", LIMIT)
		.param("format", "xml");
		
		return builder.buildGet();
	 }
	
	@Override
	public HttpAction getNextMessage() {
		popped = true;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return !popped;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		Pattern p1 = Pattern.compile("title=\"(.*?)\"");
		Matcher m1 = p1.matcher(response);

		while (m1.find()) {
			results.add(m1.group(1));
		}
		
		return super.processReturningText(response, action);
	 }
	

	public List<String> getResult() {
		return results;
	}
	
}
