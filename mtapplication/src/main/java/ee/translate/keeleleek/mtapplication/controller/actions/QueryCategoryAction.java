package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class QueryCategoryAction extends MWAction {
	
	public static final int LIMIT = 50;
	
	private String cmcontinue;
	private String category;
	private List<String> articles = new ArrayList<>();
	private List<String> categories = new ArrayList<>();
	
	private HttpAction action;
	private boolean popped = false;
	
	
	public QueryCategoryAction(String category, String cmcontinue) {
		if (category.indexOf(':') == -1) category = "Category:" + category;
		this.category = category;
		this.cmcontinue = cmcontinue;
		action = getAction();
	}
	
	public QueryCategoryAction(String list) {
		this(list, null);
	}
	
	public QueryCategoryAction next() {
		if (cmcontinue == null) return null;
		return new QueryCategoryAction(category, cmcontinue);
	}
	
	
	public HttpAction getAction()
	 {
		RequestBuilder builder = new RequestBuilder(MediaWiki.URL_API)
			.param("action", "query")
			.param("list", "categorymembers")
			.param("cmtitle", MediaWiki.urlEncode(category))
			.param("cmprop", "title")
			.param("cmlimit", LIMIT)
			.param("format", "xml");
		
		if (cmcontinue != null) builder.param("cmcontinue", MediaWiki.urlEncode(cmcontinue));
		
		System.out.println(builder.buildGet());
		
		return builder.buildGet();
	 }
	
	@Override
	public HttpAction getNextMessage() {
		popped = true;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return !popped;
	}

	@Override
	public String processReturningText(String response, HttpAction action)
	 {
		Pattern p3 = Pattern.compile("cmcontinue=\"(.*?)\"");
		Matcher m3 = p3.matcher(response);
		if (m3.find()) cmcontinue = m3.group(1);
		else cmcontinue = null;
		
		Pattern p0 = Pattern.compile("<cm (.*?) />");
		Matcher m0 = p0.matcher(response);

		while (m0.find()) {
			String member = m0.group(1);

			Pattern p1 = Pattern.compile("title=\"(.*?)\"");
			Matcher m1 = p1.matcher(member);
			
			Pattern p2 = Pattern.compile("ns=\"(.*?)\"");
			Matcher m2 = p2.matcher(member);
			
			m2.find();
			String ns = m2.group(1);
			
			m1.find();
			String title = m1.group(1);
			
			if (ns.equals("0")) articles.add(title);
			else if (ns.equals("14")) categories.add(title);
		}
		
		return super.processReturningText(response, action);
	 }
	

	public List<String> getArticles() {
		return articles;
	}
	
	public List<String> getCategories() {
		return categories;
	}
	
	public String getCmcontinue() {
		return cmcontinue;
	}
	
}
