package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import java.util.List;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.PullingPreferences;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.PluginFX;
import ee.translate.keeleleek.mtapplication.view.pages.PullingPageMediator;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.util.Callback;


public class PullingPageMediatorFX extends PullingPageMediator {

	@FXML
	private TableView<PluginFX> pluginTable;
	@FXML
	private TableColumn<PluginFX, String> pluginNameColumn;
	@FXML
	private TableColumn<PluginFX, String> pluginVersionColumn;

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		pluginNameColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PluginFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<PluginFX, String> insert) {
				return insert.getValue().name;
			}
		});
		
		pluginVersionColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PluginFX,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<PluginFX, String> insert) {
				return insert.getValue().version;
			}
		});
	 }

	
	
	/* ******************
	 *                  *
	 *       Page       *
	 *                  *
	 ****************** */
	@Override
	public void open(PullingPreferences preferences)
	 {
		String langCode = MinorityTranslateModel.preferences().display().getGUILangCode();
		List<String> pluginNames = MinorityTranslateModel.pull().getPluginNames(langCode);
		for (String name : pluginNames) {
			String version = MinorityTranslateModel.pull().getPlugin(langCode, name).getPullVersion().toString();
			pluginTable.getItems().add(new PluginFX(name, version));
		}
	 }
	
	@Override
	public PullingPreferences close()
	 {
		return new PullingPreferences();
	 }

	
}
