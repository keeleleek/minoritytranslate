package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import java.util.List;

import ee.translate.keeleleek.mtapplication.view.dialogs.SimpleDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.ExposableConfirmFX;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class SimpleDialogMediatorFX extends SimpleDialogMediator implements ExposableConfirmFX {
	
	@FXML
	protected Label titleLabel;

	@FXML
	protected TextField titleEdit;
	@FXML
	protected ComboBox<String> langCombo;

	@FXML
	protected ContextMenu titleMenu;
	@FXML
	protected ContextMenu langNameSuggestionsContextMenu;
	
	protected Button confirmButton;
	
	boolean textManual = false;
	boolean langNameManual = false;

	
	// INIT
	public SimpleDialogMediatorFX(String name)
	 {
		super(name);
	 }
	
	@FXML
	private void initialize()
	 {
		titleEdit.textProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable o)
			 {
				onTitleChanged(titleEdit.getText());
				if (confirmButton != null) confirmButton.setDisable(!isConfirmable());
			 }
		});
		
		titleMenu.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event)
			 {
				if (event.getCode() == KeyCode.SPACE) event.consume();
			 }
		});
	 }
	
	public void onRegister()
	 {
		super.onRegister();
	 }
	
	@Override
	public void exposeConfirm(Button button) {
		confirmButton = button;
		confirmButton.setDisable(!isConfirmable());
	}
	
	private boolean isConfirmable()
	 {
		return !titleEdit.getText().isEmpty() && !langCombo.getItems().isEmpty();
	 }
	
	
	// INHERIT
	@Override
	protected void initHeadingLabel(String text) {
		
	}

	@Override
	protected void initNameLabel(String text) {
		titleLabel.setText(text);
	}

	@Override
	protected void initLanguages(List<String> langNames) {
		langNameManual = true;
		ObservableList<String> values = FXCollections.observableArrayList(langNames);
		langCombo.setItems(values);
		langCombo.getSelectionModel().selectFirst();
		langNameManual = false;
	}
	
	
	@Override
	protected String getName() {
		return titleEdit.getText();
	}
	
	@Override
	protected String getLangName() {
		return langCombo.getValue();
	}

	
	@Override
	protected void updateTitle(String text)
	 {
		textManual = true;
		titleEdit.setText(text);
		titleEdit.positionCaret(text.length());
		textManual = false;
	 }
	
	@Override
	protected void suggestTitles(String[] results)
	 {
		titleMenu.getItems().clear();
		
		for (int i = 0; i < results.length; i++) {
			
			final String title = results[i];
			
			MenuItem item = new MenuItem(title);
			item.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event)
				 {
					onTitleSelected(title);
					event.consume();
				 }
			});
			
			titleMenu.getItems().add(item);
			
		}
		
		if (results.length > 0){
			titleMenu.show(titleEdit, Side.BOTTOM, 0, 0);
		} else {
			titleMenu.hide();
		}
	 }
	
	
	@Override
	protected void startEditing()
	 {
		titleEdit.requestFocus();
		titleEdit.positionCaret(titleEdit.getText().length());
	 }

    
}
