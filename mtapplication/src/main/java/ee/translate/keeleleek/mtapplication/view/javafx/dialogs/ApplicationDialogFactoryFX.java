package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory.ConfirmDecline;

public interface ApplicationDialogFactoryFX {

	public ConfirmDecline showConfirmDeclineDialog(Mediator mediator, String title, String header, String ok, String cancel);
	public void showCloseDialog(Mediator mediator, String title, String header, String close);
	
}
