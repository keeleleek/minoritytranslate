package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.DisplayPreferences;

public class ChangePreferencesProgramCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		DisplayPreferences preferences = (DisplayPreferences) notification.getBody();
		
		boolean restart = false;
		
		DisplayPreferences program = MinorityTranslateModel.preferences().display();
		if (!preferences.getGUILangCode().equals(program.getGUILangCode())) restart = true;
		else if (!preferences.getFontSize().equals(program.getFontSize())) restart = true;
		
		MinorityTranslateModel.preferences().changeProgram(preferences);

		if (restart) sendNotification(Notifications.GUI_RESTART);
	 }
	
}
