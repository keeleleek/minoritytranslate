package ee.translate.keeleleek.mtapplication.controller.populating;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;

public class OpenAddCategoryCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		getFacade().sendNotification(Notifications.WINDOW_ADD_CATEGORY_OPEN, true);
	 }
	
}
