package ee.translate.keeleleek.mtapplication.model.content;

import java.util.HashMap;

public class QidMeta {

	private String qid;
	private HashMap<String, String> langlinks = null;
	
	
	// INIT
	public QidMeta(String qid, HashMap<String, String> langlinks)
	 {
		if (qid == null) throw new NullPointerException("qid cannot be null");
		this.qid = qid;
		this.langlinks = langlinks;
	 }

	
	// DATA
	public String getQid() {
		return qid;
	}
	
	public HashMap<String, String> getLanglinks() {
		return langlinks;
	}
	
	
}
