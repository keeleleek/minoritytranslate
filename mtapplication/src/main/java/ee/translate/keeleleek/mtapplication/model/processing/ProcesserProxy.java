package ee.translate.keeleleek.mtapplication.model.processing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.processing.processers.OldLanglinksProcesser;
import ee.translate.keeleleek.mtapplication.model.processing.processers.RegexCollectProcesser;
import ee.translate.keeleleek.mtapplication.model.processing.processers.RegexReplaceProcesser;
import ee.translate.keeleleek.mtapplication.model.processing.processers.VariableProcesser;

public class ProcesserProxy extends Proxy {

	public final static String NAME = "{FFB15C82-48F1-434C-989A-06DF5DE18D25}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(ProcesserProxy.class);

	
	/* ******************
	 *                  *
	 *  Initialization  *
	 *                  *
	 ****************** */
	public ProcesserProxy() {
		super(NAME, "");
	}

	
	
	/* ******************
	 *                  *
	 *    Processing    *
	 *                  *
	 ****************** */
	public String collectReplaceProcess(String srcLangCode, String dstLangCode, String srcTitle, String srcText, String dstTitle, String dstText) throws PatternSyntaxException
	 {
		//LOGGER.info("Pullprocessing " + srcTitle + " to " + dstTitle);

		String resultText = "";
		List<RegexCollect> collects;
		ArrayList<RegexReplace> replaces;
		
		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(dstLangCode);
		switch (wikiType) {
		case REGULAR:
		case INCUBATOR:
			collects = MinorityTranslateModel.preferences().collects().filterCollects(srcLangCode, dstLangCode);
			replaces = MinorityTranslateModel.preferences().replaces().filterReplaces(srcLangCode, dstLangCode);
			HashMap<String, String> variableMap = new HashMap<>();
			resultText = srcText;
			resultText = RegexReplaceProcesser.process(srcTitle, resultText, dstTitle, dstText, collects, replaces, variableMap);
			resultText = VariableProcesser.process(dstLangCode, dstTitle, resultText, variableMap);
			break;
			
		case UNKNOWN:
			resultText = srcText;
			break;
			
		}
		
		return resultText;
	 }
	
	public String pasteProcess(String dstLangCode, String dstTitle, String dstText, String pasteText) throws PatternSyntaxException
	 {
		//LOGGER.info("Pasteprocessing from " + article);

		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(dstLangCode);
		switch (wikiType) {
		case REGULAR:
		case INCUBATOR:
			return RegexCollectProcesser.process(pasteText, dstTitle, dstText, MinorityTranslateModel.preferences().collects().filterCollects(dstLangCode, dstLangCode));
		
		default:
			return "";
		}
	 }
	
	
	/* ******************
	 *                  *
	 *     Handling     *
	 *                  *
	 ****************** */
	public boolean handleProcess(MinorityArticle article)
	 {
		LOGGER.info("Postprocessing " + article);
		
		Reference ref = article.getRef();
		
		boolean processed = false;
		
		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(ref.getLangCode());
		switch (wikiType) {
		case REGULAR:
			break;
		
		case INCUBATOR:
			processed = OldLanglinksProcesser.process(article) || processed;
			break;
		
		default:
			break;
		}
		
		if (processed) MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_ARTICLE_PROCESSED, ref);
		
		return processed;
	  }
	
	
	
}
