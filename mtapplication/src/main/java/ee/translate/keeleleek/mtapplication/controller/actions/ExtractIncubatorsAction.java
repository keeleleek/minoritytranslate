package ee.translate.keeleleek.mtapplication.controller.actions;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.jwbf.core.actions.RequestBuilder;
import net.sourceforge.jwbf.core.actions.util.HttpAction;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.util.MWAction;

public class ExtractIncubatorsAction extends MWAction {

	private String text;
	
	private HashMap<String, String> langNames = new HashMap<>();
	private HashMap<String, String> prefixes = new HashMap<>();
	
	private HttpAction action;
	
	
	public ExtractIncubatorsAction(String text) {
		this.text = text;
		action = getAction();
	}
	
	public HttpAction getAction()
	 {
		HttpAction action = new RequestBuilder(MediaWiki.URL_API)
		.param("action", "expandtemplates")
		.param("text", MediaWiki.urlEncode(text))
		.param("prop", "wikitext")
		.param("format", "xml")
		.buildGet();
		return action;
	 }
	
	@Override
	public HttpAction getNextMessage() {
		HttpAction action = this.action;
		this.action = null;
		return action;
	}

	@Override
	public boolean hasMoreMessages() {
		return action != null;
	}
	
	@Override
	public String processReturningText(String response, HttpAction action)
	 {
//		System.out.println("RESPONSE=" + response);
		
		String key = "=== Wikipedia ===";
		
		//response = MediaWiki.urlDecode(response);
		
		int i = response.indexOf(key) + key.length();
		int j = response.indexOf("===", i);
		if (i == -1 || j == -1) return super.processReturningText(response, action);
		response = response.substring(i, j);
		
		Pattern pLangCode = Pattern.compile(Pattern.quote("(wp/") + "(.+?)" + Pattern.quote(")"));
		
		String[] allSplit = response.split(Pattern.quote("|-"));
		for (String row : allSplit) {
			
			String[] rowSplit = row.split(Pattern.quote("\n|"));
			
			if (rowSplit.length != 7) continue;
		
			Matcher mLangCode = pLangCode.matcher(rowSplit[1]);
			if (!mLangCode.find()) continue;
			
			String langCode = mLangCode.group(1);
			String langName = rowSplit[6].replace("\n", "").trim();
			
			int s = langName.lastIndexOf('/');
			if (s != -1) langName = langName.substring(s + 1).trim();

			s = langName.lastIndexOf(',');
			if (s != -1) langName = langName.substring(s + 1).trim();
			
			if (langName.isEmpty()) continue;

			langNames.put(langCode, langName.toLowerCase());
			prefixes.put(langCode, "Wp/" + langCode);
			
		}
		
		return super.processReturningText(response, action);
	 }

	
	public HashMap<String, String> getLangNames() {
		return langNames;
	}
	
	public HashMap<String, String> getPrefixes() {
		return prefixes;
	}
	
	
}
