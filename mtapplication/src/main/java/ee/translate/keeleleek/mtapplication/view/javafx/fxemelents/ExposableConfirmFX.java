package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import javafx.scene.control.Button;

public interface ExposableConfirmFX {

	void exposeConfirm(Button button);

}
