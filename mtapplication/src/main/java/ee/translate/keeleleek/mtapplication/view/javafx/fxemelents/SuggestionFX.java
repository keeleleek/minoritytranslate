package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import javafx.beans.property.StringProperty;

public interface SuggestionFX {

	public StringProperty title();
	
	public StringProperty translated(String langCode);
	
}
