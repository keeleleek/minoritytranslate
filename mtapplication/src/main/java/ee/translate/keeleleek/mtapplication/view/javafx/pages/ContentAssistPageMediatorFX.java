package ee.translate.keeleleek.mtapplication.view.javafx.pages;

import ee.translate.keeleleek.mtapplication.model.preferences.ContentAssistPreferences;
import ee.translate.keeleleek.mtapplication.view.pages.ContentAssistPageMediator;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;


public class ContentAssistPageMediatorFX extends ContentAssistPageMediator {

	@FXML
	private CheckBox insertAutomaticCheckbox;
	@FXML
	private CheckBox searchLinksCheckbox;
	@FXML
	private CheckBox translateWikilinksCheckbox;
	@FXML
	private CheckBox translateTemplatesCheckbox;
	
	
	// IMPLEMENT
	@Override
	public void onRegister()
	 {
		super.onRegister();
		
	 }

	@FXML
	protected void initialize()
	 {
		
	 }

	
	// PAGE
	@Override
	public void open(ContentAssistPreferences preferences)
	 {
		insertAutomaticCheckbox.setSelected(preferences.getInsertSingleAuto());
		searchLinksCheckbox.setSelected(preferences.getSearchLinks());
		translateWikilinksCheckbox.setSelected(preferences.getTranslateWikilinks());
		translateTemplatesCheckbox.setSelected(preferences.getTranslateTemplates());
	 }
	
	@Override
	public ContentAssistPreferences close()
	 {
		return new ContentAssistPreferences(insertAutomaticCheckbox.isSelected(), searchLinksCheckbox.isSelected(), translateWikilinksCheckbox.isSelected(), translateTemplatesCheckbox.isSelected());
	 }
	
	
	// ACTIONS
	@FXML
	private void onAutomaticInsertCheck() {
		
	}

	
}
