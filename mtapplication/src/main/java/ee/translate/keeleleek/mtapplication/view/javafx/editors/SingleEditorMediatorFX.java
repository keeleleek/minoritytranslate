package ee.translate.keeleleek.mtapplication.view.javafx.editors;

import ee.translate.keeleleek.mtapplication.Notifications;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitPane;

public class SingleEditorMediatorFX extends EditorMediatorFX {

	@FXML
	private SplitPane previewEditSplit;
	@FXML
	private Separator bottomSeparator;
	
	private double dividerPosition = 0.5;
	
	
	
	// INIT
	public SingleEditorMediatorFX(EditorMode mode) {
		super(mode);
	}

	@FXML
	public void initialize()
	 {
		super.initialize();
		
		Platform.runLater(new Runnable() { // workaround for disable not working when window not visible
	        @Override
	        public void run()
	         {
	        	setDividersDisabled(previewEditSplit, true);
	         }
	    });
	 }
	
	@Override
	public void setTabView(EditorView view)
	 {
		switch (view) {
		case EDIT:
			previewView.setVisible(false);
			bottomSeparator.setVisible(false);
			textEdit.setVisible(true);
			setDividersDisabled(previewEditSplit, true);
			dividerPosition = previewEditSplit.getDividerPositions()[0];
			previewEditSplit.setDividerPositions(1.0);
			previewButton.setSelected(false);
			break;
		
		case PREVIEW:
			previewView.setVisible(true);
			bottomSeparator.setVisible(true);
			textEdit.setVisible(true);
			setDividersDisabled(previewEditSplit, false);
			previewEditSplit.setDividerPositions(dividerPosition);
			previewButton.setSelected(true);
			break;

		default:
			break;
		}
	 }
	
	
	// EVENTS
	@Override
	public void onAfterTextUpdate()
	 {
		sendNotification(Notifications.ENGINE_REQUEST_FETCH_PREVIEW, getReference());
	 }
	
	
	// HELPERS
	private static void setDividersDisabled(SplitPane split, boolean disable)
	 {
		for (Node divider : split.lookupAll(".split-pane-divider")) {
			divider.setMouseTransparent(disable);
		}
	 }

}
