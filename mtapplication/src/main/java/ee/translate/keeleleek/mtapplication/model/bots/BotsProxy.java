package ee.translate.keeleleek.mtapplication.model.bots;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.controller.actions.AllLanguages;
import ee.translate.keeleleek.mtapplication.controller.actions.FetchPreviewAction;
import ee.translate.keeleleek.mtapplication.controller.actions.LinkTitles;
import ee.translate.keeleleek.mtapplication.controller.actions.QueryPageAction;
import ee.translate.keeleleek.mtapplication.controller.actions.RandomList;
import ee.translate.keeleleek.mtapplication.controller.actions.Redirects;
import ee.translate.keeleleek.mtapplication.controller.actions.SearchAction;
import ee.translate.keeleleek.mtapplication.controller.suggestions.SuggestCategoryCommand;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.wikis.WikisProxy;
import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.actions.queries.CategoryMembersFull;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;
import net.sourceforge.jwbf.mediawiki.contentRep.CategoryItem;

public class BotsProxy extends Proxy {
	
	public final static String NAME = "{0DC2A35B-291E-4BA2-AB53-6F36C09D63AA}";
	
	public final static String COMMONS_LANG_CODE = "commons";
	public final static String META_LANG_CODE = "meta";
	
	private static Logger LOGGER = LoggerFactory.getLogger(BotsProxy.class);

	private final static String[] INVALID_LANG_CODES = new String[]{"tly", "pdt", "kk-cyrl", "gsw", "egl", "lus", "tg-latn", "rif", "fit", "ks-deva", "frc", "jam", "brh", "niu", "ku-arab", "ota", "kk-tr", "crh-cyrl", "bbc", "ike-latn", "kk-arab", "tt-latn", "sli", "vro", "sdc", "ike-cans", "lfn", "bcc", "sr-ec", "sma", "zh-mo", "azb", "zh-my", "de-at", "hif-latn", "mai", "sei", "loz", "sr-el", "arn", "arq", "zh-hant", "zh-hans", "ary", "ruq-latn", "nl-informal", "ks-arab", "ko-kp", "be-tarask", "kk-latn", "jut", "hil", "rup", "zh-cn", "vmf", "ruq", "liv", "lzh", "crh-latn", "lrc", "de-ch", "sgs", "kbd-cyrl", "shi-latn", "aln", "ug-latn", "prg", "tzm", "cps", "shi-tfng", "lzz", "hrx", "tt-cyrl", "zh-sg", "shi", "tru", "khw", "ku-latn", "en-gb", "qug", "en-ca", "yue", "ug-arab", "rwr", "de-formal", "kiu", "avk", "rgn", "grc", "vot", "dtp", "anp", "gan-hans", "inh", "krj", "kri", "tokipona", "zh-hk", "pt-br", "sat", "bho", "kk-kz", "bbc-latn", "tcy", "kk-cn", "gan-hant", "gom-latn", "tg-cyrl", "ruq-cyrl", "bqi"};
	
	
	private HashMap<String, MediaWikiBot> bots = new HashMap<>();
	private MediaWikiBot wikidataBot;
	private MediaWikiBot incubatorBot;
	private MediaWikiBot commonsBot;
	private MediaWikiBot metaBot = null;
	
	private HashMap<String, Article> articles = new HashMap<>();
	
	
	// INIT
	public BotsProxy() {
		super(NAME);
	}
	

	// BOTS:
	/**
	 * Gets the bot for the given language.
	 * 
	 * @param langCode language code.
	 * @return bot for given language
	 */
	public MediaWikiBot fetchBot(String langCode)
	 {
		if (langCode.equals(COMMONS_LANG_CODE)) return getCommonsBot();
		if (langCode.equals(META_LANG_CODE)) return getMetaBot();
		return MinorityTranslateModel.login().fetchBot(langCode);
	 }

	public Article fetchArticle(String langCode, String title)
	 {
		MediaWikiBot bot = fetchBot(langCode);

		WikiType wikiType = MinorityTranslateModel.wikis().getWikiType(langCode);
		switch (wikiType) {
		case REGULAR:
			return bot.getArticle(title);
			
		case INCUBATOR:
			return bot.getArticle(WikisProxy.prefixTitle(title, WikisProxy.prefix(langCode)));
			
		default:
			LOGGER.error("Failed to fetch article, because the wikiType is unknown for langCode " + langCode);
			return bot.getArticle(title);
		}
	 }
	
	/**
	 * Checks if a bot is loaded.
	 * 
	 * @param langCode language code
	 * @return true if loaded
	 */
	public boolean isBotLoaded(String langCode) {
		return bots.containsKey(langCode);
	}

	/**
	 * Unloads a bot.
	 * 
	 * @param langCode language code.
	 */
	public void unloadBot(String langCode) {
		bots.remove(langCode);
	}
	
	/**
	 * Gets the wikidata bot.
	 * 
	 * @return wikidata bot
	 */
	public MediaWikiBot getWikidataBot() {
		if (wikidataBot == null) {
			wikidataBot = new MediaWikiBot("https://www.wikidata.org/w/");
		}
		return wikidataBot;
	}

	/**
	 * Gets the incubator bot.
	 * 
	 * @return incubator bot
	 */
	public MediaWikiBot getIncubatorBot() {
		if (incubatorBot == null) {
			incubatorBot = new MediaWikiBot("https://incubator.wikimedia.org/w/");
		}
		return incubatorBot;
	}

	/**
	 * Gets the commons bot.
	 * 
	 * @return commons bot
	 */
	public MediaWikiBot getCommonsBot() {
		if (commonsBot == null) {
			commonsBot = new MediaWikiBot("https://commons.wikimedia.org/w/");
		}
		return commonsBot;
	}

	/**
	 * Gets the meta bot.
	 * 
	 * @return meta bot
	 */
	public MediaWikiBot getMetaBot() {
		if (metaBot == null) {
			metaBot = new MediaWikiBot("https://meta.wikimedia.org/w/");
		}
		return metaBot;
	}
	
	/**
	 * Gets to bots.
	 * 
	 * @return to bots
	 */
	public MediaWikiBot[] fetchToBots()
	 {
		List<String> toLangCodes = MinorityTranslateModel.preferences().languages().filterDstLangCodes();

		MediaWikiBot[] bots = new MediaWikiBot[toLangCodes.size() + 1];
		for (int i = 0; i < toLangCodes.size(); i++) {
			bots[i] = fetchBot(toLangCodes.get(i));
		}
		bots[toLangCodes.size()] = getWikidataBot();
		
		return bots;
	 }
	
	/**
	 * Checks whether a bot is logged in.
	 * 
	 * @param langCode language code
	 * @return true if logged in
	 */
	public boolean isLoggedIn(String langCode) {
		return fetchBot(langCode).isLoggedIn();
	}
	
	
	// ARTICLES:
	/**
	 * Retrieves an article.
	 * 
	 * @param langCode language code
	 * @param title title
	 * @return article
	 */
	public Article retrieveArticle(String langCode, String title) {
		Article article = articles.get(langCode + ":" + title);
		if (article == null){
			MediaWikiBot bot = fetchBot(langCode);
			article = bot.getArticle(title);
			articles.put(langCode + ":" + title, article);
		}
		return article;
	}
	
	/**
	 * Gets category article titles.
	 * 
	 * @param langCode language code
	 * @param category category name
	 * @param remaining remaining depth
	 * @return category article titles
	 */
	public List<String> getCategoryTitles(String langCode, String category, int remaining)
	 {
		ArrayList<String> titles = new ArrayList<>();
		CategoryMembersFull articleMembers = new CategoryMembersFull(fetchBot(langCode), category, MediaWiki.NS_MAIN);
		for (CategoryItem member : articleMembers) {
			titles.add(member.getTitle());
		}
		
		if (remaining <= 0) return titles;
		remaining--;
		
		CategoryMembersFull categoryMembers = new CategoryMembersFull(fetchBot(langCode), category, MediaWiki.NS_CATEGORY);
		for (CategoryItem member : categoryMembers){
			String title = member.getTitle();
			int i = title.indexOf(":");
			if (i != -1) title = title.substring(i + 1);
			titles.addAll(getCategoryTitles(langCode, title, remaining));
		}
		
		return titles;
	 }
	
	public List<String> getCategoryTitles(String langCode, String category)
	 {
		return getCategoryTitles(langCode, category, MinorityTranslateModel.preferences().content().getCategoryDepth());
	 }
	
	
	// TEXT:
	/**
	 * Retrieves text.
	 * 
	 * @param langCode language code
	 * @param title article title
	 * @return article text
	 */
	public String retrieveText(String langCode, String title) {
		if (title.length() == 0) return "";
		return retrieveArticle(langCode, title).getText();
	}
	
	
	// WIKIDATA:
	/**
	 * Links two interlanguage titles.
	 * 
	 * @param fromLangCode from language code
	 * @param fromTitle from language title
	 * @param toLangCode to language code
	 * @param toTitle to language title
	 * @return true if successful
	 */
	public boolean linkInterlang(String fromLangCode, String fromTitle, String toLangCode, String toTitle)
	 {
		MediaWikiBot wikidataBot = getWikidataBot();
		LinkTitles linkTitles = new LinkTitles(fromLangCode, fromTitle, toLangCode, toTitle);
		
		wikidataBot.getPerformedAction(linkTitles);
		
		return true;
	 }
	
	
	// SEARCH:
	/**
	 * Requests category suggestions.
	 * 
	 * @param langCode language code
	 * @param search search term
	 * @param target suggestions target
	 */
	public void requestCategorySuggestions(final String langCode, final String search, final SuggestCategoryCommand target) {
		final int namespace = MediaWiki.NS_CATEGORY;
		
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					MediaWikiBot bot = fetchBot(langCode);
					SearchAction suggestions = new SearchAction(search, namespace);
					bot.getPerformedAction(suggestions);
					target.handleSuggestions(search, namespace, suggestions.getResults());
				} catch (IllegalStateException e) {
					LOGGER.warn("Failed to fetch category suggestions: " + e.getMessage() + "!");
				}
			}
		});
		thread.start();
	}
	
	
	// LANGUAGES:
	/**
	 * Retrieves all wiki languages.
	 * 
	 * @return all wiki languages
	 */
	public HashMap<String, String> retrieveLanguages()
	 {
		LOGGER.info("Retrieveing languages");
		
		AllLanguages action = new AllLanguages();
		MediaWikiBot bot = getWikidataBot();
		bot.getPerformedAction(action);
		
		// TODO: Handle missing languages:
		HashMap<String, String> languages = action.getLanguages();
		
		for (int i = 0; i < INVALID_LANG_CODES.length; i++) {
			languages.remove(INVALID_LANG_CODES[i]);
		}
		
		LOGGER.info("Retrieved " + languages.size() + " languages");
		
		return action.getLanguages();
	}
	
	
	// PARSING:
	public String parseText(String langCode, String text)
	 {
		FetchPreviewAction action = new FetchPreviewAction(text);
		MediaWikiBot bot = fetchBot(langCode);
		bot.getPerformedAction(action);
		return action.getParsedText();
	}
	
	
	// REDIRECT:
	public List<String> getRedirects(String langCode, List<String> titles) {
		Redirects action = new Redirects(titles);
		MediaWikiBot bot = fetchBot(langCode);
		bot.getPerformedAction(action);
		
		return action.getTranslated();
	}
	
	
	// LISTS:
	/**
	 * Fills the list.
	 * 
	 * @param langCode language code
	 * @param listName list name
	 * @param offset offset String; null if none
	 * @param list list to fill
	 * @return offset String; null if none
	 */
	public String fillList(String langCode, String listName, String offset, List<String> list) {
		QueryPageAction action = new QueryPageAction(listName, offset);
		MediaWikiBot bot = fetchBot(langCode);
		bot.getPerformedAction(action);
		list.addAll(action.getResult());
		return action.getQpoffset();
	}
	

	public void fillRandomList(String langCode, List<String> list) {
		RandomList action = new RandomList();
		MediaWikiBot bot = fetchBot(langCode);
		bot.getPerformedAction(action);
		list.addAll(action.getResult());
	}

	
	
	// LEGACY ID UTILITY:
	public static String id(String title, String langCode) {
		return title + ":" + langCode;
	}

	public static String langCode(String id) {
		return id.substring(id.indexOf(":") + 1, id.length());
	}

	public static String title(String id) {
		return id.substring(0, id.indexOf(":"));
	}
	
}
