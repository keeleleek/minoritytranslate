package ee.translate.keeleleek.mtapplication.controller.gui;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.view.javafx.application.ApplicationMediatorFX;

public class PrepareGUICommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		Object translateWindow = notification.getBody();
		
		Mediator mediator;
		
		mediator = new ApplicationMediatorFX(translateWindow);
		
		getFacade().registerMediator(mediator);
	 }
	
}
