package ee.translate.keeleleek.mtapplication.view.pages;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.model.preferences.TranslatePreferences;


public abstract class TranslatePageMediator extends Mediator implements PreferencesPage<TranslatePreferences> {

	public final static String NAME = "{18A94B9D-97C1-488D-A750-705F981747A7}";
	
	
	// INIT
	public TranslatePageMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister() {
		
	}

	
}
