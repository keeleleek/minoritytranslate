package ee.translate.keeleleek.mtapplication.model.timer;

import java.util.HashMap;
import java.util.List;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.controller.actions.FetchNamespaces;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.WikiType;
import ee.translate.keeleleek.mtapplication.model.content.Namespace;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class TimerProxy extends Proxy implements Runnable {

	public final static String NAME = "{8CBF53F1-F56A-414E-8A39-3E47168F476B}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(TimerProxy.class);
	
	public final static long TIMEOUT = 500;
	public final static int AUTOSAVE_TICKS = 20;
	public final static int FETCH_SUGGESTION_LISTS_TICKS = 5;
	public final static int UPDATE_WIKIS_PERIOD = 1209600000; // 2 weeks
	
	private boolean autosave = false;
	private int autosaveTick = 0;

	private int fetchSuggestionListsTick = 1;
	
	private boolean workConnectedOnce = true;
	private boolean run = false;

	
	// INIT
	public TimerProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister() {
		run = true;
	}
	
	@Override
	public void onRemove() {
		run = false;
	}

	public void start()
	 {
		Thread thread = new Thread(this);
		thread.setDaemon(true);
		thread.start();
	 }
	
	
	// WORK
	@Override
	public void run()
	 {
		try {
			while (run) {
				work();
				Thread.sleep(TIMEOUT);
			}
		}
		catch (InterruptedException e) {
			LOGGER.error("Thread sleep failure!", e);
		}
		catch (Exception e) {
			LOGGER.warn("Tick failed!", e);
			start();
		}
	 }
	
	private void work()
	 {
		lookBusy();
		
		autosaveTick();
		
		if (MinorityTranslateModel.connection().hasConnection()) {
			
			if (workConnectedOnce) {
				
				workWikis();
				
				workUserlists();
				
				workConnectedOnce = false;
			}
			
			workNamespaces(); // fetch namespaces
			
			workFetchSuggestionListsTick();
			
		}
		
		lookBusy();
	 }

	private void lookBusy()
	 {
		
		//MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_DOWNLOADER_BUSY, busy);
	 }
	
	
	// AUTOSAVE
	public void scheduleAutosave()
	 {
		autosave = true;
	 }

	public void cancelAutosave()
	 {
		autosave = false;
	 }

	private void autosaveTick()
	 {
		autosaveTick++;
		if (autosaveTick < AUTOSAVE_TICKS) return;
		autosaveTick = 0;
		
		if (!autosave) return;
		autosave = false;
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run()
			 {
				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.TIMER_SIGNAL_AUTOSAVE);
				
				LOGGER.info("Autosave complete");
			 }
		});
		
		LOGGER.info("Autosaving content");
		thread.start();
	 }

	private void workWikis()
	 {
		long current = System.currentTimeMillis();
		
		if (current - MinorityTranslateModel.wikis().getWikisUpdateTime() < UPDATE_WIKIS_PERIOD) return;
		
		LOGGER.info("Downloading wiki information");
		
		MinorityTranslateModel.wikis().download();
	 }

	private void workUserlists()
	 {
		LOGGER.info("Downloading userlists information");
		
		MinorityTranslateModel.lists().downloadUserlists();
	 }

	private void workFetchSuggestionListsTick()
	 {
		fetchSuggestionListsTick++;
		if (fetchSuggestionListsTick < FETCH_SUGGESTION_LISTS_TICKS) return;
		fetchSuggestionListsTick = 0;
		
		// LOGGER.info("Fetching suggestion list elements");
		
		MinorityTranslateModel.lists().fetchSuggestions();
	 }
	
	private void workNamespaces()
	 {
		List<String> langCodes = MinorityTranslateModel.preferences().languages().getAllLangCodes();
		
		for (String langCode : langCodes) {
			
			if (!MinorityTranslateModel.wikis().isNamespaceMapped(langCode) && MinorityTranslateModel.wikis().getWikiType(langCode) == WikiType.REGULAR) {
				
				LOGGER.info("Fetching namespaces for " + langCode + " wiki");
				
				FetchNamespaces action = new FetchNamespaces();
				MediaWikiBot bot = MinorityTranslateModel.bots().fetchBot(langCode);
				bot.getPerformedAction(action);
				
				HashMap<Namespace, String> mappings = action.getMappings();
				MinorityTranslateModel.wikis().mapNamespaces(langCode, mappings);
				
			}
			
		}
	 }

	
}
