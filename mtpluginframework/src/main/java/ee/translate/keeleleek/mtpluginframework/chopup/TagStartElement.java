package ee.translate.keeleleek.mtpluginframework.chopup;

public class TagStartElement extends ArticleElement {

	private String name = "";
	
	public TagStartElement() {
		super(Type.TAG_START);
	}
	
	@Override
	public void init(String text) {
		this.name = text;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	protected String asCode() {
		return "<" + name + ">";
	}
	
}
