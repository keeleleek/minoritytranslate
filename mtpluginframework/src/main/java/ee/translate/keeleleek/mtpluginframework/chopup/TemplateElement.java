package ee.translate.keeleleek.mtpluginframework.chopup;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class TemplateElement extends ArticleElement {

	private String name = "";
	private ArrayList<TemplateParameterElement> parameters = new ArrayList<>();
	private boolean multiline = false;
	
	private boolean nameTouched = false;
	
	
	// INIT
	public TemplateElement() {
		super(Type.TEMPLATE);
	}
	
	@Override
	public void init(String text)
	 {
		multiline = text.contains("\n");
		
		String[] split = text.split(Pattern.quote("|"));
		
		name = split[0].trim();
		if (name.contains("\n")) name = name.replace("\n", "");
		
		for (int i = 0; i < split.length - 1; i++) {
			
			createParameter((i + 1), split[i + 1]);
			
		}
	 }

	private TemplateParameterElement createParameter(int index, String str)
	 {
		String name = "" + index;
		String value = str;
		
		if (value.contains("\n")) value = value.replace("\n", " ");

		int j = value.indexOf('=');
		if (j != -1) {
			name = value.substring(0, j);
			value = value.substring(j + 1);
		}
		
		TemplateParameterElement element = new TemplateParameterElement(name.trim(), value.trim());
		parameters.add(element);
		
		return element;
	 }

	public TemplateParameterElement createParameter(String str)
	 {
		return createParameter(parameters.size(), str);
	 }
	
	public void removeParameter(String name)
	 {
		for (int i = 0; i < parameters.size(); i++) {
			if (parameters.get(i).getName().equals(name)) {
				parameters.remove(i);
				i--;
			}
		}
	 }
	
	public int getParameterCount() {
		return parameters.size();
	}
	
	public TemplateParameterElement getParameter(int i) {
		return parameters.get(i);
	}

	private TemplateParameterElement findParameter(String name)
	 {
		for (TemplateParameterElement parameter : parameters) {
			if (parameter.getName().equals(name)) return parameter;
		}
		return null;
	 }

	private ArrayList<String> collectParameterNames()
	 {
		ArrayList<String> names = new ArrayList<>();
		for (TemplateParameterElement parameter : parameters) {
			names.add(parameter.getName());
		}
		return names;
	 }
	
	private int findParamCount()
	 {
		int max = 0;
		int duds = 0;
		
		for (TemplateParameterElement parameter : parameters) {
			
			if (parameter.getName().isEmpty()) {
				duds++;
				continue;
			}
			
			String name = parameter.getName();
			try {
				Integer index = Integer.parseInt(name);
				if (index > max) max = index;
			} catch (NumberFormatException e) { }
		}
		
		if (parameters.size() - duds > max) max = parameters.size() - duds;
		
		return max;
	 }
	
	@Override
	protected String asCode()
	 {
		ArrayList<String> names = collectParameterNames();
		
		int paramCount = findParamCount();
		
		String[] parameters = new String[paramCount];

		// insert unnamed
		for (int i = 0; i < paramCount; i++) {
			
			String name = "" + (i + 1);
			
			TemplateParameterElement parameter = findParameter(name);
			
			if (parameter != null) {
				parameters[i] = parameter.getValue();
				names.remove(name);
			}
			
		}

		// insert named and empty
		for (int i = 0; i < parameters.length; i++) {
			
			if (parameters[i] != null) continue;
			
			if (names.size() != 0) {
				String name = names.remove(0);
				
				if (name.isEmpty()) {
					i--;
					continue;
				}
				
				TemplateParameterElement parameter = findParameter(name);
				
				if (parameter != null) {
					parameters[i] = parameter.getName() + " = " + parameter.getValue();
					continue;
				}
				
			}
			
			parameters[i] = "";
			
		}

		// trim values
		for (int i = 0; i < parameters.length; i++) {
			
			String parameter = parameters[i];
			
			int j = parameter.indexOf('=');
			if (j == -1) continue;
			
			int k = parameter.indexOf('=', j + 1);
			if (k == -1) continue;
			
			parameter = parameter.substring(0, k).trim();
			
			parameters[i] = parameter;
			
		}
		
		// template start
		StringBuilder builder = new StringBuilder();
		String sep = multiline ? "\n | " : "|";
		builder.append("{{");
		builder.append(name);
		
		// parameters
		for (int i = 0; i < parameters.length; i++) {
			
			builder.append(sep);
			builder.append(parameters[i]);
			
		}
		
		// template end
		if (multiline) builder.append("\n");
		builder.append("}}");
		return builder.toString();
	 }
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
		this.nameTouched = true;
	}
	
	public boolean isNameTouched() {
		return nameTouched;
	}
	
	public TemplateParameterElement changeParameterName(String oldName, String newNameValue)
	 {
		if (oldName.isEmpty() && newNameValue.isEmpty()) return null;
		
		TemplateParameterElement parameter = findParameter(oldName);
		if (parameter != null)  parameter.setName(newNameValue);
		
		return parameter;
	 }
	
}
