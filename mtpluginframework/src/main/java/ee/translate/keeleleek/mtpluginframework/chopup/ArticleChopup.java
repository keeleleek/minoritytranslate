package ee.translate.keeleleek.mtpluginframework.chopup;

import java.util.Stack;

import ee.translate.keeleleek.mtpluginframework.chopup.ArticleElement.Type;

public class ArticleChopup {

	private static char[] TERMINATORS = new char[]{' ', '.', ',' , '!', '?', '\n', '-', '\'', '(', ')', '[', ']', '{', '}', '<', '>'};
	
	
	public static RootElement chopup(String text)
	 {
		if (text.length() == 0) return new RootElement();
		
		text = text.replace("<br>", "<br />"); // <br> is weird
		
		ArticleElement parent;
		ArticleElement child;
		
		Stack<ArticleElement> current = new Stack<>();
		RootElement root = new RootElement();
		ArticleElement first = new TextBlockElement();
		root.append(first);
		
		current.add(root);
		current.add(first);
		
		int i = 0;
		for (; i < text.length() - 1; i++) {
			
			switch (text.charAt(i)) {
			case '{':
				if (text.charAt(i + 1) == '{') {
					parent = current.peek();
					child = new TemplateElement();
					current.push(child);
					parent.append(child);
					
					i+= 1;
					continue;
				}
				break;

			case '[':
				if (text.charAt(i + 1) == '[') {
					parent = current.peek();
					child = new WikilinkElement();
					current.push(child);
					parent.append(child);
					
					i+= 1;
					continue;
				}
				break;

			case '<':
				// comment
				if (text.charAt(i + 1) == '!') {
					i+= 1;
					continue;
				}
					
				// end tag
				if (text.charAt(i + 1) == '/') {
					child = new TagEndElement();
					if (current.peek().type() == Type.TAG_CONTENT) current.pop();
					
					parent = current.peek();
					current.push(child);
					parent.append(child);
					
					i+= 1;
					continue;
				}
				// start tag
				else {
					child = new TagStartElement();
					
					parent = current.peek();
					current.push(child);
					parent.append(child);
					
					i+= 0;
					continue;
				}

			case '}':
				if (text.charAt(i + 1) == '}' && current.peek().type() == Type.TEMPLATE) {
					current.pop();
					
					i+= 1;
					continue;
				}
				break;

			case ']':
				if (text.charAt(i + 1) == ']' && current.peek().type() == Type.WIKILINK) {
					WikilinkElement wikilink = (WikilinkElement) current.pop();
					
					String attachment = collectAttached(text, i + 2);
					if (!attachment.isEmpty()) {
						wikilink.attach(attachment);
						i+= attachment.length();
					}
					
					i+= 1;
					continue;
				}
				break;

			case '\n':
				if (current.peek().type() == Type.TEXT_BLOCK) {
					current.pop();
					
					parent = current.peek();
					child = new TextBlockElement();
					current.push(child);
					parent.append(child);
					
					i+= 0;
					continue;
					
				}
				break;

			case '>':
				if (current.peek().type() == Type.TAG_START) {
					current.pop();
					
					if (i != 0 && text.charAt(i - 1) == '/') {
						// all done
						continue;
					}
					
					parent = current.peek();
					child = new TagContentElement();
					current.push(child);
					parent.append(child);
					
					i+= 0;
					continue;
				}
				
				else if (current.peek().type() == Type.TAG_END) {
					current.pop();
					
					i+= 0;
					continue;
				}
				break;

			default:
				break;
			}
			
			current.peek().append(text.charAt(i));
			
		}
		
		if (i == text.length() - 1) {
			ArticleElement last = current.pop();
			last.append(text.charAt(text.length() - 1));
		}
		
		root.dump();
		
		return root;
	 }
	
	
	public static String trimNamespace(String link) {
		int j = link.indexOf(':');
		if (j != -1) return link.substring(j + 1);
		return link;
	}
	
	public static String collectAttached(String text, int i)
	 {
		StringBuilder builder = new StringBuilder();
		
		for (int j = i; j < text.length(); j++) {
			
			char c = text.charAt(j);
			
			for (int k = 0; k < TERMINATORS.length; k++) {
				if (TERMINATORS[k] == c) return builder.toString();
			}
			
			builder.append(c);
		}
		
		return builder.toString();
	 }


}
